<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatMonogLibroTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatMonogLibroTable Test Case
 */
class CatMonogLibroTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatMonogLibroTable
     */
    public $CatMonogLibro;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatMonogLibro'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatMonogLibro') ? [] : ['className' => CatMonogLibroTable::class];
        $this->CatMonogLibro = TableRegistry::getTableLocator()->get('CatMonogLibro', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatMonogLibro);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
