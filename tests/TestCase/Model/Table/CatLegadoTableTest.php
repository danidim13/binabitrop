<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatLegadoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatLegadoTable Test Case
 */
class CatLegadoTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatLegadoTable
     */
    public $CatLegado;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatLegado'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatLegado') ? [] : ['className' => CatLegadoTable::class];
        $this->CatLegado = TableRegistry::getTableLocator()->get('CatLegado', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatLegado);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
