<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatFavoritosUsuarioTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatFavoritosUsuarioTable Test Case
 */
class CatFavoritosUsuarioTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatFavoritosUsuarioTable
     */
    public $CatFavoritosUsuario;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatFavoritosUsuario'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatFavoritosUsuario') ? [] : ['className' => CatFavoritosUsuarioTable::class];
        $this->CatFavoritosUsuario = TableRegistry::getTableLocator()->get('CatFavoritosUsuario', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatFavoritosUsuario);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
