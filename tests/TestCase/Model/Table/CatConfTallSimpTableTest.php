<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatConfTallSimpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatConfTallSimpTable Test Case
 */
class CatConfTallSimpTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatConfTallSimpTable
     */
    public $CatConfTallSimp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatConfTallSimp'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatConfTallSimp') ? [] : ['className' => CatConfTallSimpTable::class];
        $this->CatConfTallSimp = TableRegistry::getTableLocator()->get('CatConfTallSimp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatConfTallSimp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
