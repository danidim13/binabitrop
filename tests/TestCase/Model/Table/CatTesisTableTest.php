<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatTesisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatTesisTable Test Case
 */
class CatTesisTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatTesisTable
     */
    public $CatTesis;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatTesis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatTesis') ? [] : ['className' => CatTesisTable::class];
        $this->CatTesis = TableRegistry::getTableLocator()->get('CatTesis', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatTesis);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
