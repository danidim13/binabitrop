<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatRegistroTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatRegistroTable Test Case
 */
class CatRegistroTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatRegistroTable
     */
    public $CatRegistro;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatRegistro',
        'app.CatConfTallerSimposio',
        'app.CatMonogLibro',
        'app.CatPMonogCapLib',
        'app.CatSeriada',
        'app.CatTesis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatRegistro') ? [] : ['className' => CatRegistroTable::class];
        $this->CatRegistro = TableRegistry::getTableLocator()->get('CatRegistro', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatRegistro);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generalSearch method
     *
     * @return void
     */
    public function testGeneralSearch()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test titleSearch method
     *
     * @return void
     */
    public function testTitleSearch()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test authorSearch method
     *
     * @return void
     */
    public function testAuthorSearch()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test locationSearch method
     *
     * @return void
     */
    public function testLocationSearch()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test keywordSearch method
     *
     * @return void
     */
    public function testKeywordSearch()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
