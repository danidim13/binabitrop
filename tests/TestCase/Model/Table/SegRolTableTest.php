<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SegRolTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SegRolTable Test Case
 */
class SegRolTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SegRolTable
     */
    public $SegRol;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SegRol'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SegRol') ? [] : ['className' => SegRolTable::class];
        $this->SegRol = TableRegistry::getTableLocator()->get('SegRol', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SegRol);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
