<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatSeriadaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatSeriadaTable Test Case
 */
class CatSeriadaTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatSeriadaTable
     */
    public $CatSeriada;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatSeriada'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatSeriada') ? [] : ['className' => CatSeriadaTable::class];
        $this->CatSeriada = TableRegistry::getTableLocator()->get('CatSeriada', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatSeriada);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
