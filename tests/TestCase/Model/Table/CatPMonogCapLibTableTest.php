<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatPMonogCapLibTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatPMonogCapLibTable Test Case
 */
class CatPMonogCapLibTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatPMonogCapLibTable
     */
    public $CatPMonogCapLib;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatPMonogCapLib'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatPMonogCapLib') ? [] : ['className' => CatPMonogCapLibTable::class];
        $this->CatPMonogCapLib = TableRegistry::getTableLocator()->get('CatPMonogCapLib', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatPMonogCapLib);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
