<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatPMonogCapTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatPMonogCapTable Test Case
 */
class CatPMonogCapTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatPMonogCapTable
     */
    public $CatPMonogCap;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatPMonogCap'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatPMonogCap') ? [] : ['className' => CatPMonogCapTable::class];
        $this->CatPMonogCap = TableRegistry::getTableLocator()->get('CatPMonogCap', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatPMonogCap);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
