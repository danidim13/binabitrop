<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SegPermisosRolTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SegPermisosRolTable Test Case
 */
class SegPermisosRolTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SegPermisosRolTable
     */
    public $SegPermisosRol;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SegPermisosRol'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SegPermisosRol') ? [] : ['className' => SegPermisosRolTable::class];
        $this->SegPermisosRol = TableRegistry::getTableLocator()->get('SegPermisosRol', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SegPermisosRol);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
