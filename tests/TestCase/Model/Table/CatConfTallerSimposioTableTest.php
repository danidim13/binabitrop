<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatConfTallerSimposioTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatConfTallerSimposioTable Test Case
 */
class CatConfTallerSimposioTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CatConfTallerSimposioTable
     */
    public $CatConfTallerSimposio;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CatConfTallerSimposio'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CatConfTallerSimposio') ? [] : ['className' => CatConfTallerSimposioTable::class];
        $this->CatConfTallerSimposio = TableRegistry::getTableLocator()->get('CatConfTallerSimposio', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatConfTallerSimposio);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
