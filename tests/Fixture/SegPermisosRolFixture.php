<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SegPermisosRolFixture
 */
class SegPermisosRolFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'seg_permisos_rol';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'PERMISOS_ROL' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'ROL_FK' => ['type' => 'string', 'length' => '30', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'PERMISOS_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['PERMISOS_ROL'], 'length' => []],
            'SEG_PERMISO_ROL_FK_PERMISO' => ['type' => 'foreign', 'columns' => ['PERMISOS_FK'], 'references' => ['BINABITROP.SEG_PERMISO', 'ID_PERMISO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
            'SEG_PERMISO_ROL_FK_ROL' => ['type' => 'foreign', 'columns' => ['ROL_FK'], 'references' => ['BINABITROP.SEG_ROL', 'ID_ROL'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'PERMISOS_ROL' => 1,
                'ROL_FK' => 'Lorem ipsum dolor sit amet',
                'PERMISOS_FK' => 1
            ],
        ];
        parent::init();
    }
}
