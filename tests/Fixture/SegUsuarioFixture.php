<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SegUsuarioFixture
 */
class SegUsuarioFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'seg_usuario';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'ID_USUARIO' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'NOMBRE_USUARIO' => ['type' => 'string', 'length' => '30', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ROL_FK' => ['type' => 'string', 'length' => '30', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NOMBRE' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'PRIMER_APELLIDO' => ['type' => 'string', 'length' => '50', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'CORREO_ELECTRONICO' => ['type' => 'string', 'length' => '100', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'BCRYPT_HASH' => ['type' => 'string', 'length' => '60', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TOKEN' => ['type' => 'string', 'length' => '7', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ACTIVO' => ['type' => 'string', 'length' => '1', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        '_constraints' => [
            'SYS_C008946' => ['type' => 'unique', 'columns' => ['NOMBRE_USUARIO'], 'length' => []],
            'primary' => ['type' => 'primary', 'columns' => ['ID_USUARIO'], 'length' => []],
            'SEG_USUARIO_FK_ROL' => ['type' => 'foreign', 'columns' => ['ROL_FK'], 'references' => ['BINABITROP.SEG_ROL', 'ID_ROL'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'ID_USUARIO' => 1,
                'NOMBRE_USUARIO' => 'Lorem ipsum dolor sit amet',
                'ROL_FK' => 'Lorem ipsum dolor sit amet',
                'NOMBRE' => 'Lorem ipsum dolor sit amet',
                'PRIMER_APELLIDO' => 'Lorem ipsum dolor sit amet',
                'CORREO_ELECTRONICO' => 'Lorem ipsum dolor sit amet',
                'BCRYPT_HASH' => 'Lorem ipsum dolor sit amet',
                'TOKEN' => 'Lorem',
                'ACTIVO' => 'L'
            ],
        ];
        parent::init();
    }
}
