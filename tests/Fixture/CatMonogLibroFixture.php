<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatMonogLibroFixture
 */
class CatMonogLibroFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_monog_libro';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'REGISTRO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'COLACION_M_C' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'EDITORIAL' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'SERIES_MONOG' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['REGISTRO_FK'], 'length' => []],
            'CAT_MONOG_LIBRO_FK' => ['type' => 'foreign', 'columns' => ['REGISTRO_FK'], 'references' => ['BINABITROP.CAT_REGISTRO', 'ID_REGISTRO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'REGISTRO_FK' => 1,
                'COLACION_M_C' => 'Lorem ipsum dolor sit amet',
                'EDITORIAL' => 'Lorem ipsum dolor sit amet',
                'SERIES_MONOG' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
