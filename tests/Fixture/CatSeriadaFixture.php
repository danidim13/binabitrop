<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatSeriadaFixture
 */
class CatSeriadaFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_seriada';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'REGISTRO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'TITULO_SERIADA2' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ISSN' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'MENC_DE_LAS_PART' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['REGISTRO_FK'], 'length' => []],
            'CAT_SERIADA_FK' => ['type' => 'foreign', 'columns' => ['REGISTRO_FK'], 'references' => ['BINABITROP.CAT_REGISTRO', 'ID_REGISTRO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'REGISTRO_FK' => 1,
                'TITULO_SERIADA2' => 'Lorem ipsum dolor sit amet',
                'ISSN' => 'Lorem ipsum dolor sit amet',
                'MENC_DE_LAS_PART' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
