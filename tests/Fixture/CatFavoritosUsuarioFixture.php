<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatFavoritosUsuarioFixture
 */
class CatFavoritosUsuarioFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_favoritos_usuario';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'USUARIO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'REGISTRO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['USUARIO_FK', 'REGISTRO_FK'], 'length' => []],
            'CAT_FAV_USUARIO_FK_USUARIO' => ['type' => 'foreign', 'columns' => ['USUARIO_FK'], 'references' => ['BINABITROP.SEG_USUARIO', 'ID_USUARIO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
            'CAT_FAV_USUARIO_FK_REGISTRO' => ['type' => 'foreign', 'columns' => ['REGISTRO_FK'], 'references' => ['BINABITROP.CAT_REGISTRO', 'ID_REGISTRO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'USUARIO_FK' => 1,
                'REGISTRO_FK' => 1
            ],
        ];
        parent::init();
    }
}
