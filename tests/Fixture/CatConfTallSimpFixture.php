<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatConfTallSimpFixture
 */
class CatConfTallSimpFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_conf_tall_simp';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'REGISTRO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'COLACION_M_C' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'REUNION_ES' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['REGISTRO_FK'], 'length' => []],
            'CAT_CONF_TALL_SIMP_FK' => ['type' => 'foreign', 'columns' => ['REGISTRO_FK'], 'references' => ['BINABITROP.CAT_REGISTRO', 'ID_REGISTRO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'REGISTRO_FK' => 1,
                'COLACION_M_C' => 'Lorem ipsum dolor sit amet',
                'REUNION_ES' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
