<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatPMonogCapFixture
 */
class CatPMonogCapFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_p_monog_cap';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'REGISTRO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'COLACION_M_C' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'EDITORIAL' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ISBN_S' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITUL_PADR_M_C' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['REGISTRO_FK'], 'length' => []],
            'CAT_P_MONOG_CAP_LIB_FK' => ['type' => 'foreign', 'columns' => ['REGISTRO_FK'], 'references' => ['BINABITROP.CAT_REGISTRO', 'ID_REGISTRO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'REGISTRO_FK' => 1,
                'COLACION_M_C' => 'Lorem ipsum dolor sit amet',
                'EDITORIAL' => 'Lorem ipsum dolor sit amet',
                'ISBN_S' => 'Lorem ipsum dolor sit amet',
                'TITUL_PADR_M_C' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
