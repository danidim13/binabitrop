<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatLegadoFixture
 */
class CatLegadoFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_legado';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'REGISTRO_FK' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'FECHA_INGRESO' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ISO_PAIS_ORIG' => ['type' => 'string', 'length' => '2', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'FECHA_PUBLICACION' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ESTADO_PROCESO' => ['type' => 'string', 'length' => '400', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TIPO_ADQUISICION' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'SUPLIDOR' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'FECHA_REGISTRO_DE_AUTORIDADES' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'FECHA_MODIFICACION_DEL_REG' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'SOLICTANTE_S' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'REFERENCIA_S_DE_VEASE' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'REFERENCIA_S_DE_VEASE2' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO_PARALELO_ING' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO_PARALELO_ESP' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO_PARALELO_FRN' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULOS_PARALELOS' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO_S_ANTERIOR_ES' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO_S_POSTERIOR_ES' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO_PARALELO_OTRO_IDIOMA' => ['type' => 'string', 'length' => '2000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'VER_TMB_EDIC_EN_OTRO_IDIOMA' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'VERSIONES_EN_OTROS_IDIOMAS' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'PROYECTO_S' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'PRECIO' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'OTRAS_INSTITU_ASOC' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NUMERO_S_DE_ACCESO' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NUMERO_DE_COPIAS' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NUMERO_COPIAS_ORDENADAS' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NUMERO_REGISTRO_DE_AUTORI' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NOTAS_ADQUISICION' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'EDICION' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ESTADO_SOLICITUD' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'N_S_DOCUMENTOS_S' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NOMBRE_Y_DIR_SUPLIDOR' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NOMBRE_S_PORTERIOR_ES' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NOMBRE_S_ANTERIOR_ES' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'FECHA_DEL_RECLAMO' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ESTADO_PROCESAMIENTO' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'IDIOMA_DEL_ANALISIS' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'N_REG_CENTRO_PARTICIPANTE' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'N_REG_VERSION_OTROS_IDIOMAS' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ENCABEZ_TEMATICO_GEN_' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ISBN' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NIVEL_BIBLIOG' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NIVEL_BIBLIO_DOC_PADRE' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ID_BASE' => ['type' => 'integer', 'length' => '10', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['REGISTRO_FK'], 'length' => []],
            'CAT_LEGADO_FK' => ['type' => 'foreign', 'columns' => ['REGISTRO_FK'], 'references' => ['BINABITROP.CAT_REGISTRO', 'ID_REGISTRO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'REGISTRO_FK' => 1,
                'FECHA_INGRESO' => 'Lorem ipsum dolor sit amet',
                'ISO_PAIS_ORIG' => 'Lo',
                'FECHA_PUBLICACION' => 'Lorem ipsum dolor sit amet',
                'ESTADO_PROCESO' => 'Lorem ipsum dolor sit amet',
                'TIPO_ADQUISICION' => 'Lorem ipsum dolor sit amet',
                'SUPLIDOR' => 'Lorem ipsum dolor sit amet',
                'FECHA_REGISTRO_DE_AUTORIDADES' => 'Lorem ipsum dolor sit amet',
                'FECHA_MODIFICACION_DEL_REG' => 'Lorem ipsum dolor sit amet',
                'SOLICTANTE_S' => 'Lorem ipsum dolor sit amet',
                'REFERENCIA_S_DE_VEASE' => 'Lorem ipsum dolor sit amet',
                'REFERENCIA_S_DE_VEASE2' => 'Lorem ipsum dolor sit amet',
                'TITULO_PARALELO_ING' => 'Lorem ipsum dolor sit amet',
                'TITULO_PARALELO_ESP' => 'Lorem ipsum dolor sit amet',
                'TITULO_PARALELO_FRN' => 'Lorem ipsum dolor sit amet',
                'TITULOS_PARALELOS' => 'Lorem ipsum dolor sit amet',
                'TITULO_S_ANTERIOR_ES' => 'Lorem ipsum dolor sit amet',
                'TITULO_S_POSTERIOR_ES' => 'Lorem ipsum dolor sit amet',
                'TITULO_PARALELO_OTRO_IDIOMA' => 'Lorem ipsum dolor sit amet',
                'VER_TMB_EDIC_EN_OTRO_IDIOMA' => 'Lorem ipsum dolor sit amet',
                'VERSIONES_EN_OTROS_IDIOMAS' => 'Lorem ipsum dolor sit amet',
                'PROYECTO_S' => 'Lorem ipsum dolor sit amet',
                'PRECIO' => 'Lorem ipsum dolor sit amet',
                'OTRAS_INSTITU_ASOC' => 'Lorem ipsum dolor sit amet',
                'NUMERO_S_DE_ACCESO' => 'Lorem ipsum dolor sit amet',
                'NUMERO_DE_COPIAS' => 'Lorem ipsum dolor sit amet',
                'NUMERO_COPIAS_ORDENADAS' => 'Lorem ipsum dolor sit amet',
                'NUMERO_REGISTRO_DE_AUTORI' => 'Lorem ipsum dolor sit amet',
                'NOTAS_ADQUISICION' => 'Lorem ipsum dolor sit amet',
                'EDICION' => 'Lorem ipsum dolor sit amet',
                'ESTADO_SOLICITUD' => 'Lorem ipsum dolor sit amet',
                'N_S_DOCUMENTOS_S' => 'Lorem ipsum dolor sit amet',
                'NOMBRE_Y_DIR_SUPLIDOR' => 'Lorem ipsum dolor sit amet',
                'NOMBRE_S_PORTERIOR_ES' => 'Lorem ipsum dolor sit amet',
                'NOMBRE_S_ANTERIOR_ES' => 'Lorem ipsum dolor sit amet',
                'FECHA_DEL_RECLAMO' => 'Lorem ipsum dolor sit amet',
                'ESTADO_PROCESAMIENTO' => 'Lorem ipsum dolor sit amet',
                'IDIOMA_DEL_ANALISIS' => 'Lorem ipsum dolor sit amet',
                'N_REG_CENTRO_PARTICIPANTE' => 'Lorem ipsum dolor sit amet',
                'N_REG_VERSION_OTROS_IDIOMAS' => 'Lorem ipsum dolor sit amet',
                'ENCABEZ_TEMATICO_GEN_' => 'Lorem ipsum dolor sit amet',
                'ISBN' => 'Lorem ipsum dolor sit amet',
                'NIVEL_BIBLIOG' => 'Lorem ipsum dolor sit amet',
                'NIVEL_BIBLIO_DOC_PADRE' => 'Lorem ipsum dolor sit amet',
                'ID_BASE' => 1
            ],
        ];
        parent::init();
    }
}
