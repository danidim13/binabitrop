<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatRegistroFixture
 */
class CatRegistroFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cat_registro';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'ID_REGISTRO' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'DISPONIBILIDAD' => ['type' => 'string', 'length' => '1', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ACRON_CENTRO_PARTI' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'AFILIACION' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'AUTOR_INST_DOC_PADRE' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'AUTOR_INSTIT' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'AUTOR_PERSONAL_DOC_PADRE' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'AUTORES_INSTIT' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'AUTORES_PERSONAL' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'DESCRIPT_GEOG' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'DESCRIPT_LOCALES' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'DESCRIPT_PRIMAR' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'DESCRIPT_SECUND' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'DOCUMENTALISTA_S' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'FECHA_INGRESO_REGISTRO' => ['type' => 'string', 'length' => '500', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'FECHA_PUBL_EDICION_FROM_ISO' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'FECHA_PUBL_IN_EXT' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'IDREG' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ISSN2' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'MENC_DE_LAS_PART' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'N_REGISTROS_DE_LAS_PARTES' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'RESUMEN' => ['type' => 'text', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'collate' => null],
        'SIGNATURA_TOPOGRAFICA' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TIPO_DE_MATERIAL' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'TITULO' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'UBICACION' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NIVEL_BIBLIOGR_DOC_PADRE' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NIVEL_BIBLIOGRAFICO' => ['type' => 'string', 'length' => '100', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'NOTAS' => ['type' => 'string', 'length' => '4000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'URL_OTS' => ['type' => 'string', 'length' => '1000', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'PDF' => ['type' => 'string', 'length' => '50', 'null' => true, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        'ACTIVO' => ['type' => 'string', 'length' => '1', 'null' => false, 'default' => null, 'comment' => null, 'precision' => null, 'fixed' => null, 'collate' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['ID_REGISTRO'], 'length' => []],
            'CAT_REG_USUARIO_FK' => ['type' => 'foreign', 'columns' => ['DOCUMENTALISTA_S'], 'references' => ['BINABITROP.SEG_USUARIO', 'ID_USUARIO'], 'update' => 'setNull', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'ID_REGISTRO' => 1,
                'DISPONIBILIDAD' => 'L',
                'ACRON_CENTRO_PARTI' => 'Lorem ipsum dolor sit amet',
                'AFILIACION' => 'Lorem ipsum dolor sit amet',
                'AUTOR_INST_DOC_PADRE' => 'Lorem ipsum dolor sit amet',
                'AUTOR_INSTIT' => 'Lorem ipsum dolor sit amet',
                'AUTOR_PERSONAL_DOC_PADRE' => 'Lorem ipsum dolor sit amet',
                'AUTORES_INSTIT' => 'Lorem ipsum dolor sit amet',
                'AUTORES_PERSONAL' => 'Lorem ipsum dolor sit amet',
                'DESCRIPT_GEOG' => 'Lorem ipsum dolor sit amet',
                'DESCRIPT_LOCALES' => 'Lorem ipsum dolor sit amet',
                'DESCRIPT_PRIMAR' => 'Lorem ipsum dolor sit amet',
                'DESCRIPT_SECUND' => 'Lorem ipsum dolor sit amet',
                'DOCUMENTALISTA_S' => 1,
                'FECHA_INGRESO_REGISTRO' => 'Lorem ipsum dolor sit amet',
                'FECHA_PUBL_EDICION_FROM_ISO' => 'Lorem ipsum dolor sit amet',
                'FECHA_PUBL_IN_EXT' => 'Lorem ipsum dolor sit amet',
                'IDREG' => 'Lorem ipsum dolor sit amet',
                'ISSN2' => 'Lorem ipsum dolor sit amet',
                'MENC_DE_LAS_PART' => 'Lorem ipsum dolor sit amet',
                'N_REGISTROS_DE_LAS_PARTES' => 'Lorem ipsum dolor sit amet',
                'RESUMEN' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'SIGNATURA_TOPOGRAFICA' => 'Lorem ipsum dolor sit amet',
                'TIPO_DE_MATERIAL' => 'Lorem ipsum dolor sit amet',
                'TITULO' => 'Lorem ipsum dolor sit amet',
                'UBICACION' => 'Lorem ipsum dolor sit amet',
                'NIVEL_BIBLIOGR_DOC_PADRE' => 'Lorem ipsum dolor sit amet',
                'NIVEL_BIBLIOGRAFICO' => 'Lorem ipsum dolor sit amet',
                'NOTAS' => 'Lorem ipsum dolor sit amet',
                'URL_OTS' => 'Lorem ipsum dolor sit amet',
                'PDF' => 'Lorem ipsum dolor sit amet',
                'ACTIVO' => 'L'
            ],
        ];
        parent::init();
    }
}
