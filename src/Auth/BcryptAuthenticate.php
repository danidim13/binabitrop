<?php
namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Base Authentication class for Bcrypt  
 * @author Daniel Díaz
 */

 class BcryptAuthenticate extends BaseAuthenticate
 {
    /**
     * Determina si los datos suministrados autentican correctamente a un usuario.
     * @param Cake\Http\ServerRequest $request Request instance
     * @param Cake\Http\Response $response Response instance
     * @return \App\Model\Entity\SegUsuario|bool Returns the user or false if authentication failed.
     * @author Daniel Díaz
     */
    public function authenticate(ServerRequest $request, Response $response)
    {
        // debug('hola');
        if (!$this->_checkFields($request)) {
            return false;
        }
        // debug('hola2');
        return $this->getUser($request);
    }

    /**
     * Verifica que se hayan llenado los campos requeridos para la autenticación.
     * @param Cake\Http\ServerRequest $request Request instance
     * @return bool
     * @author Daniel Díaz
     */
    protected function _checkFields(ServerRequest $request)
    {
        // debug($request);
        foreach (['username', 'password'] as $field) {
            $value = $request->getData($field);
            if (empty($value) || !is_string($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Busca un usuario en la tabla userModel por su nombre de usuario y contraseña
     * Si no es una combinación válida retorna false.
     * @param Cake\Http\ServerRequest $request Request instance
     * @return \App\Model\Entity\SegUsuario|bool
     * @author Daniel Díaz
     */
    public function getUser(ServerRequest $request)
    {
        $username_plain = $request->getData('username');
        $password_plain = $username_plain . $request->getData('password') . 'BINABITROP';

        $user = $this->_query($username_plain)->first();
        // debug($user);
        // debug($password_plain);
        // die();

        $field = $this->_config['fields']['password'];
        $password_hash = $user[$field];
        unset($user[$field]);

        // debug($user);
        // die();

        $hash_ok = password_verify($password_plain, $password_hash);

        // debug($hash_ok);
        // debug($password_hash);
        // debug($password_plain);
        // die();
        
        if ($hash_ok && !empty($user)) {
            return $user;
        } else {
            return false;
        }   
    }

    /**
     * Hasheo de contraseñas.
     * El hash se construye a partir de una combinación del nombre de usuario,
     * la contraseña y un string constante para la aplicación.
     * @param string $username Nombre de usuario
     * @param string $password Contraseña en texto plano
     * @return string|bool Hash de la contraseña o False si hay algún error
     * @author Daniel Díaz
     */
    public static function password($username, $password)
    {

        $password_plain = $username . $password . 'BINABITROP';
        $timeTarget = 0.05; // 50 milliseconds hashing time target
        $cost = 9; // default cost 10
        
        do {
            $cost++;
            $start = microtime(true);
            password_hash($password_plain, PASSWORD_BCRYPT, ['cost' => $cost]);
            $end = microtime(true);
        } while (($end - $start) < $timeTarget);

        $BCRYPT_HASH = password_hash($password_plain, PASSWORD_BCRYPT, ['cost' => $cost]);
        return $BCRYPT_HASH; 
    }

 }

?>
