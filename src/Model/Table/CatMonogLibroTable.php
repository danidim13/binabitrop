<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatMonogLibro Model
 *
 * @method \App\Model\Entity\CatMonogLibro get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatMonogLibro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatMonogLibro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatMonogLibro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatMonogLibro saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatMonogLibro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatMonogLibro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatMonogLibro findOrCreate($search, callable $callback = null, $options = [])
 */
class CatMonogLibroTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_MONOG_LIBRO');
        $this->setDisplayField('REGISTRO_FK');
        $this->setPrimaryKey('REGISTRO_FK');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        $validator
            ->scalar('COLACION_M_C')
            ->maxLength('COLACION_M_C', 1000)
            ->allowEmptyString('COLACION_M_C');

        $validator
            ->scalar('EDITORIAL')
            ->maxLength('EDITORIAL', 2000)
            ->allowEmptyString('EDITORIAL');

        $validator
            ->scalar('SERIES_MONOG')
            ->maxLength('SERIES_MONOG', 2000)
            ->allowEmptyString('SERIES_MONOG');

        return $validator;
    }
}
