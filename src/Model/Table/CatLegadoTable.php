<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatLegado Model
 *
 * @method \App\Model\Entity\CatLegado get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatLegado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatLegado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatLegado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatLegado saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatLegado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatLegado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatLegado findOrCreate($search, callable $callback = null, $options = [])
 */
class CatLegadoTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_LEGADO');
        $this->setDisplayField('REGISTRO_FK');
        $this->setPrimaryKey('REGISTRO_FK');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        $validator
            ->scalar('FECHA_INGRESO')
            ->maxLength('FECHA_INGRESO', 500)
            ->allowEmptyString('FECHA_INGRESO');

        $validator
            ->scalar('ISO_PAIS_ORIG')
            ->maxLength('ISO_PAIS_ORIG', 2)
            ->requirePresence('ISO_PAIS_ORIG', 'create')
            ->allowEmptyString('ISO_PAIS_ORIG', false);

        $validator
            ->scalar('FECHA_PUBLICACION')
            ->maxLength('FECHA_PUBLICACION', 500)
            ->allowEmptyString('FECHA_PUBLICACION');

        $validator
            ->scalar('ESTADO_PROCESO')
            ->maxLength('ESTADO_PROCESO', 400)
            ->allowEmptyString('ESTADO_PROCESO');

        $validator
            ->scalar('TIPO_ADQUISICION')
            ->maxLength('TIPO_ADQUISICION', 2000)
            ->allowEmptyString('TIPO_ADQUISICION');

        $validator
            ->scalar('SUPLIDOR')
            ->maxLength('SUPLIDOR', 1000)
            ->allowEmptyString('SUPLIDOR');

        $validator
            ->scalar('FECHA_REGISTRO_DE_AUTORIDADES')
            ->maxLength('FECHA_REGISTRO_DE_AUTORIDADES', 500)
            ->allowEmptyString('FECHA_REGISTRO_DE_AUTORIDADES');

        $validator
            ->scalar('FECHA_MODIFICACION_DEL_REG')
            ->maxLength('FECHA_MODIFICACION_DEL_REG', 500)
            ->allowEmptyString('FECHA_MODIFICACION_DEL_REG');

        $validator
            ->scalar('SOLICTANTE_S')
            ->maxLength('SOLICTANTE_S', 100)
            ->allowEmptyString('SOLICTANTE_S');

        $validator
            ->scalar('REFERENCIA_S_DE_VEASE')
            ->maxLength('REFERENCIA_S_DE_VEASE', 1000)
            ->allowEmptyString('REFERENCIA_S_DE_VEASE');

        $validator
            ->scalar('REFERENCIA_S_DE_VEASE2')
            ->maxLength('REFERENCIA_S_DE_VEASE2', 1000)
            ->allowEmptyString('REFERENCIA_S_DE_VEASE2');

        $validator
            ->scalar('TITULO_PARALELO_ING')
            ->maxLength('TITULO_PARALELO_ING', 2000)
            ->allowEmptyString('TITULO_PARALELO_ING');

        $validator
            ->scalar('TITULO_PARALELO_ESP')
            ->maxLength('TITULO_PARALELO_ESP', 2000)
            ->allowEmptyString('TITULO_PARALELO_ESP');

        $validator
            ->scalar('TITULO_PARALELO_FRN')
            ->maxLength('TITULO_PARALELO_FRN', 2000)
            ->allowEmptyString('TITULO_PARALELO_FRN');

        $validator
            ->scalar('TITULOS_PARALELOS')
            ->maxLength('TITULOS_PARALELOS', 2000)
            ->allowEmptyString('TITULOS_PARALELOS');

        $validator
            ->scalar('TITULO_S_ANTERIOR_ES')
            ->maxLength('TITULO_S_ANTERIOR_ES', 2000)
            ->allowEmptyString('TITULO_S_ANTERIOR_ES');

        $validator
            ->scalar('TITULO_S_POSTERIOR_ES')
            ->maxLength('TITULO_S_POSTERIOR_ES', 2000)
            ->allowEmptyString('TITULO_S_POSTERIOR_ES');

        $validator
            ->scalar('TITULO_PARALELO_OTRO_IDIOMA')
            ->maxLength('TITULO_PARALELO_OTRO_IDIOMA', 2000)
            ->allowEmptyString('TITULO_PARALELO_OTRO_IDIOMA');

        $validator
            ->scalar('VER_TMB_EDIC_EN_OTRO_IDIOMA')
            ->maxLength('VER_TMB_EDIC_EN_OTRO_IDIOMA', 100)
            ->allowEmptyString('VER_TMB_EDIC_EN_OTRO_IDIOMA');

        $validator
            ->scalar('VERSIONES_EN_OTROS_IDIOMAS')
            ->maxLength('VERSIONES_EN_OTROS_IDIOMAS', 1000)
            ->allowEmptyString('VERSIONES_EN_OTROS_IDIOMAS');

        $validator
            ->scalar('PROYECTO_S')
            ->maxLength('PROYECTO_S', 1000)
            ->allowEmptyString('PROYECTO_S');

        $validator
            ->scalar('PRECIO')
            ->maxLength('PRECIO', 1000)
            ->allowEmptyString('PRECIO');

        $validator
            ->scalar('OTRAS_INSTITU_ASOC')
            ->maxLength('OTRAS_INSTITU_ASOC', 1000)
            ->allowEmptyString('OTRAS_INSTITU_ASOC');

        $validator
            ->scalar('NUMERO_S_DE_ACCESO')
            ->maxLength('NUMERO_S_DE_ACCESO', 100)
            ->allowEmptyString('NUMERO_S_DE_ACCESO');

        $validator
            ->scalar('NUMERO_DE_COPIAS')
            ->maxLength('NUMERO_DE_COPIAS', 4000)
            ->allowEmptyString('NUMERO_DE_COPIAS');

        $validator
            ->scalar('NUMERO_COPIAS_ORDENADAS')
            ->maxLength('NUMERO_COPIAS_ORDENADAS', 100)
            ->allowEmptyString('NUMERO_COPIAS_ORDENADAS');

        $validator
            ->scalar('NUMERO_REGISTRO_DE_AUTORI')
            ->maxLength('NUMERO_REGISTRO_DE_AUTORI', 1000)
            ->allowEmptyString('NUMERO_REGISTRO_DE_AUTORI');

        $validator
            ->scalar('NOTAS_ADQUISICION')
            ->maxLength('NOTAS_ADQUISICION', 1000)
            ->allowEmptyString('NOTAS_ADQUISICION');

        $validator
            ->scalar('EDICION')
            ->maxLength('EDICION', 1000)
            ->allowEmptyString('EDICION');

        $validator
            ->scalar('ESTADO_SOLICITUD')
            ->maxLength('ESTADO_SOLICITUD', 1000)
            ->allowEmptyString('ESTADO_SOLICITUD');

        $validator
            ->scalar('N_S_DOCUMENTOS_S')
            ->maxLength('N_S_DOCUMENTOS_S', 1000)
            ->allowEmptyString('N_S_DOCUMENTOS_S');

        $validator
            ->scalar('NOMBRE_Y_DIR_SUPLIDOR')
            ->maxLength('NOMBRE_Y_DIR_SUPLIDOR', 1000)
            ->allowEmptyString('NOMBRE_Y_DIR_SUPLIDOR');

        $validator
            ->scalar('NOMBRE_S_PORTERIOR_ES')
            ->maxLength('NOMBRE_S_PORTERIOR_ES', 1000)
            ->allowEmptyString('NOMBRE_S_PORTERIOR_ES');

        $validator
            ->scalar('NOMBRE_S_ANTERIOR_ES')
            ->maxLength('NOMBRE_S_ANTERIOR_ES', 1000)
            ->allowEmptyString('NOMBRE_S_ANTERIOR_ES');

        $validator
            ->scalar('FECHA_DEL_RECLAMO')
            ->maxLength('FECHA_DEL_RECLAMO', 100)
            ->allowEmptyString('FECHA_DEL_RECLAMO');

        $validator
            ->scalar('ESTADO_PROCESAMIENTO')
            ->maxLength('ESTADO_PROCESAMIENTO', 4000)
            ->allowEmptyString('ESTADO_PROCESAMIENTO');

        $validator
            ->scalar('IDIOMA_DEL_ANALISIS')
            ->maxLength('IDIOMA_DEL_ANALISIS', 100)
            ->allowEmptyString('IDIOMA_DEL_ANALISIS');

        $validator
            ->scalar('N_REG_CENTRO_PARTICIPANTE')
            ->maxLength('N_REG_CENTRO_PARTICIPANTE', 100)
            ->allowEmptyString('N_REG_CENTRO_PARTICIPANTE');

        $validator
            ->scalar('N_REG_VERSION_OTROS_IDIOMAS')
            ->maxLength('N_REG_VERSION_OTROS_IDIOMAS', 100)
            ->allowEmptyString('N_REG_VERSION_OTROS_IDIOMAS');

        $validator
            ->scalar('ENCABEZ_TEMATICO_GEN_')
            ->maxLength('ENCABEZ_TEMATICO_GEN_', 4000)
            ->allowEmptyString('ENCABEZ_TEMATICO_GEN_');

        $validator
            ->scalar('ISBN')
            ->maxLength('ISBN', 100)
            ->allowEmptyString('ISBN');

        $validator
            ->scalar('NIVEL_BIBLIOG')
            ->maxLength('NIVEL_BIBLIOG', 100)
            ->allowEmptyString('NIVEL_BIBLIOG');

        $validator
            ->scalar('NIVEL_BIBLIO_DOC_PADRE')
            ->maxLength('NIVEL_BIBLIO_DOC_PADRE', 100)
            ->allowEmptyString('NIVEL_BIBLIO_DOC_PADRE');

        $validator
            ->integer('ID_BASE')
            ->allowEmptyString('ID_BASE');

        return $validator;
    }
}
