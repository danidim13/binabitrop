<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SegRol Model
 *
 * @method \App\Model\Entity\SegRol get($primaryKey, $options = [])
 * @method \App\Model\Entity\SegRol newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SegRol[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SegRol|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SegRol saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SegRol patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SegRol[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SegRol findOrCreate($search, callable $callback = null, $options = [])
 */
class SegRolTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('SEG_ROL');
        $this->setDisplayField('ID_ROL');
        $this->setPrimaryKey('ID_ROL');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('ID_ROL')
            ->maxLength('ID_ROL', 30)
            ->allowEmptyString('ID_ROL', 'create');

        return $validator;
    }
}
