<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use phpDocumentor\Reflection\Types\String_;

/**
 * CatRegistro Model
 *
 * @method \App\Model\Entity\CatRegistro get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatRegistro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatRegistro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatRegistro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatRegistro saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatRegistro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatRegistro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatRegistro findOrCreate($search, callable $callback = null, $options = [])
 */
class CatRegistroTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_REGISTRO');
        $this->setDisplayField('ID_REGISTRO');
        $this->setPrimaryKey('ID_REGISTRO');
    }

    public function consultRegister($id = null){
        $registers = TableRegistry::get('CatRegistro');
        $query = $registers->find()
            ->select(['ID_REGISTRO','TITULO','AUTORES_INSTIT','AUTORES_PERSONAL','FECHA_PUBL_IN_EXT','AFILIACION','NOTAS','DESCRIPT_PRIMAR','DESCRIPT_SECUND','DESCRIPT_GEOG','DESCRIPT_LOCALES','UBICACION','RESUMEN','TIPO_DE_MATERIAL'])
            ->where(['ID_REGISTRO =' => $id]);

        return $query->first();
    }

    /**
     * Consults registers that match the search parameters
     *
     * @param string $entry keyword taken from text input to compare with search terms.
     * @return Query contains query to execute.
     * @author André Flasterstein
     */
    public function generalSearch($entry)
    {
        $registers = TableRegistry::get('CatRegistro');
        $query = $registers->find()
            ->select(['ID_REGISTRO', 'TITULO', 'AUTOR_INSTIT', 'AUTORES_PERSONAL', 'TIPO_DE_MATERIAL', 'DISPONIBILIDAD'])
            ->where(['OR' => ['TITULO LIKE' => '%' . $entry . '%', 'AUTOR_INSTIT LIKE' => '%' . $entry . '%', 'AUTORES_PERSONAL LIKE' => '%' . $entry . '%', 'UBICACION LIKE' => '%' . $entry . '%']])
            ->order(['TITULO' => 'ASC']);

        return $query;
    }

    /**
     * Consults registers that match the search parameters
     *
     * @param string $entry keyword taken from text input to compare with titles in the DB.
     * @return Query contains query to execute.
     * @author André Flasterstein
     */
    public function titleSearch($entry)
    {
        $registers = TableRegistry::get('CatRegistro');
        $query = $registers->find()
            ->select(['ID_REGISTRO', 'TITULO', 'AUTOR_INSTIT', 'AUTORES_PERSONAL', 'TIPO_DE_MATERIAL', 'DISPONIBILIDAD'])
            ->where(['TITULO LIKE' => '%' . $entry . '%'])
            ->order(['TITULO' => 'ASC']);

        return $query;
    }

    /**
     * Consults registers that match the search parameters
     *
     * @param string $entry keyword taken from text input to compare with all kinds of authors in the DB.
     * @return Query contains query to execute.
     * @author André Flasterstein
     */
    public function authorSearch($entry)
    {
        $registers = TableRegistry::get('CatRegistro');
        $query = $registers->find()
            ->select(['ID_REGISTRO', 'TITULO', 'AUTOR_INSTIT', 'AUTORES_PERSONAL', 'TIPO_DE_MATERIAL', 'DISPONIBILIDAD'])
            ->where(['OR' => ['AUTOR_INSTIT LIKE' => '%' . $entry . '%', 'AUTORES_INSTIT LIKE' => '%' . $entry . '%', 'AUTORES_PERSONAL LIKE' => '%' . $entry . '%']])
            ->order(['TITULO' => 'ASC']);

        return $query;
    }

    /**
     * Consults registers that match the search parameters
     *
     * @param string $entry keyword taken from text input to compare with locations that are in the DB.
     * @return Query contains query to execute.
     * @author André Flasterstein
     */
    public function locationSearch($entry)
    {
        $registers = TableRegistry::get('CatRegistro');
        $query = $registers->find()
            ->select(['ID_REGISTRO', 'TITULO', 'AUTOR_INSTIT', 'AUTORES_PERSONAL', 'TIPO_DE_MATERIAL', 'DISPONIBILIDAD'])
            ->where(['UBICACION LIKE' => '%' . $entry . '%'])
            ->order(['TITULO' => 'ASC']);

        return $query;
    }

    /**
     * Consults registers that match the search parameters
     *
     * @param string $entry keyword taken from text input to compare with document keywords that are in the DB.
     * @return Query contains query to execute.
     * @author André Flasterstein
     */
    public function keywordSearch($entry)
    {
        $registers = TableRegistry::get('CatRegistro');
        $query = $registers->find()
            ->select(['ID_REGISTRO', 'TITULO', 'AUTOR_INSTIT', 'AUTORES_PERSONAL', 'TIPO_DE_MATERIAL', 'DISPONIBILIDAD'])
            ->where(['OR' =>['DESCRIPT_PRIMAR LIKE' => '%' . $entry . '%', 'DESCRIPT_SECUND LIKE' => '%' . $entry . '%', 'DESCRIPT_GEOG LIKE' => '%' . $entry . '%', 'DESCRIPT_LOCALES LIKE' => '%' . $entry . '%']])
            ->order(['TITULO' => 'ASC']);

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID_REGISTRO')
            ->allowEmptyString('ID_REGISTRO', 'create');

        $validator
            ->scalar('DISPONIBILIDAD')
            ->maxLength('DISPONIBILIDAD', 1)
            ->allowEmptyString('DISPONIBILIDAD');

        $validator
            ->scalar('ACRON_CENTRO_PARTI')
            ->maxLength('ACRON_CENTRO_PARTI', 100)
            ->allowEmptyString('ACRON_CENTRO_PARTI');

        $validator
            ->scalar('AFILIACION')
            ->maxLength('AFILIACION', 1000)
            ->allowEmptyString('AFILIACION');

        $validator
            ->scalar('AUTOR_INST_DOC_PADRE')
            ->maxLength('AUTOR_INST_DOC_PADRE', 1000)
            ->allowEmptyString('AUTOR_INST_DOC_PADRE');

        $validator
            ->scalar('AUTOR_INSTIT')
            ->maxLength('AUTOR_INSTIT', 1000)
            ->allowEmptyString('AUTOR_INSTIT');

        $validator
            ->scalar('AUTOR_PERSONAL_DOC_PADRE')
            ->maxLength('AUTOR_PERSONAL_DOC_PADRE', 1000)
            ->allowEmptyString('AUTOR_PERSONAL_DOC_PADRE');

        $validator
            ->scalar('AUTORES_INSTIT')
            ->maxLength('AUTORES_INSTIT', 1000)
            ->allowEmptyString('AUTORES_INSTIT');

        $validator
            ->scalar('AUTORES_PERSONAL')
            ->maxLength('AUTORES_PERSONAL', 1000)
            ->allowEmptyString('AUTORES_PERSONAL');

        $validator
            ->scalar('DESCRIPT_GEOG')
            ->maxLength('DESCRIPT_GEOG', 4000)
            ->allowEmptyString('DESCRIPT_GEOG');

        $validator
            ->scalar('DESCRIPT_LOCALES')
            ->maxLength('DESCRIPT_LOCALES', 4000)
            ->allowEmptyString('DESCRIPT_LOCALES');

        $validator
            ->scalar('DESCRIPT_PRIMAR')
            ->maxLength('DESCRIPT_PRIMAR', 4000)
            ->allowEmptyString('DESCRIPT_PRIMAR');

        $validator
            ->scalar('DESCRIPT_SECUND')
            ->maxLength('DESCRIPT_SECUND', 4000)
            ->allowEmptyString('DESCRIPT_SECUND');

        $validator
            ->integer('DOCUMENTALISTA_S')
            ->requirePresence('DOCUMENTALISTA_S', 'create')
            ->allowEmptyString('DOCUMENTALISTA_S', false);

        $validator
            ->scalar('FECHA_INGRESO_REGISTRO')
            ->maxLength('FECHA_INGRESO_REGISTRO', 500)
            ->allowEmptyString('FECHA_INGRESO_REGISTRO');

        $validator
            ->scalar('FECHA_PUBL_EDICION_FROM_ISO')
            ->maxLength('FECHA_PUBL_EDICION_FROM_ISO', 100)
            ->allowEmptyString('FECHA_PUBL_EDICION_FROM_ISO');

        $validator
            ->scalar('FECHA_PUBL_IN_EXT')
            ->maxLength('FECHA_PUBL_IN_EXT', 100)
            ->allowEmptyString('FECHA_PUBL_IN_EXT');

        $validator
            ->scalar('IDREG')
            ->maxLength('IDREG', 100)
            ->allowEmptyString('IDREG');

        $validator
            ->scalar('ISSN2')
            ->maxLength('ISSN2', 100)
            ->allowEmptyString('ISSN2');

        $validator
            ->scalar('MENC_DE_LAS_PART')
            ->maxLength('MENC_DE_LAS_PART', 1000)
            ->allowEmptyString('MENC_DE_LAS_PART');

        $validator
            ->scalar('N_REGISTROS_DE_LAS_PARTES')
            ->maxLength('N_REGISTROS_DE_LAS_PARTES', 100)
            ->allowEmptyString('N_REGISTROS_DE_LAS_PARTES');

        $validator
            ->scalar('RESUMEN')
            ->maxLength('RESUMEN', 4000)
            ->allowEmptyString('RESUMEN');

        $validator
            ->scalar('SIGNATURA_TOPOGRAFICA')
            ->maxLength('SIGNATURA_TOPOGRAFICA', 4000)
            ->allowEmptyString('SIGNATURA_TOPOGRAFICA');

        $validator
            ->scalar('TIPO_DE_MATERIAL')
            ->maxLength('TIPO_DE_MATERIAL', 1000)
            ->allowEmptyString('TIPO_DE_MATERIAL');

        $validator
            ->scalar('TITULO')
            ->maxLength('TITULO', 1000)
            ->allowEmptyString('TITULO');

        $validator
            ->scalar('UBICACION')
            ->maxLength('UBICACION', 4000)
            ->allowEmptyString('UBICACION');

        $validator
            ->scalar('NIVEL_BIBLIOGR_DOC_PADRE')
            ->maxLength('NIVEL_BIBLIOGR_DOC_PADRE', 100)
            ->allowEmptyString('NIVEL_BIBLIOGR_DOC_PADRE');

        $validator
            ->scalar('NIVEL_BIBLIOGRAFICO')
            ->maxLength('NIVEL_BIBLIOGRAFICO', 100)
            ->allowEmptyString('NIVEL_BIBLIOGRAFICO');

        $validator
            ->scalar('NOTAS')
            ->maxLength('NOTAS', 4000)
            ->allowEmptyString('NOTAS');

        $validator
            ->scalar('URL_OTS')
            ->maxLength('URL_OTS', 1000)
            ->allowEmptyString('URL_OTS');

        $validator
            ->scalar('PDF')
            ->maxLength('PDF', 50)
            ->allowEmptyString('PDF');

        $validator
            ->scalar('ACTIVO')
            ->maxLength('ACTIVO', 1)
            ->requirePresence('ACTIVO', 'create')
            ->allowEmptyString('ACTIVO', false);

        return $validator;
    }
}
