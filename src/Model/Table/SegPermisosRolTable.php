<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SegPermisosRol Model
 *
 * @method \App\Model\Entity\SegPermisosRol get($primaryKey, $options = [])
 * @method \App\Model\Entity\SegPermisosRol newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SegPermisosRol[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SegPermisosRol|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SegPermisosRol saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SegPermisosRol patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SegPermisosRol[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SegPermisosRol findOrCreate($search, callable $callback = null, $options = [])
 */
class SegPermisosRolTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('SEG_PERMISOS_ROL');
        $this->setDisplayField('PERMISOS_ROL');
        $this->setPrimaryKey('PERMISOS_ROL');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('PERMISOS_ROL')
            ->allowEmptyString('PERMISOS_ROL', 'create');

        $validator
            ->scalar('ROL_FK')
            ->maxLength('ROL_FK', 30)
            ->requirePresence('ROL_FK', 'create')
            ->allowEmptyString('ROL_FK', false);

        $validator
            ->integer('PERMISOS_FK')
            ->requirePresence('PERMISOS_FK', 'create')
            ->allowEmptyString('PERMISOS_FK', false);

        return $validator;
    }
}
