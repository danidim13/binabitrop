<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatFavoritosUsuario Model
 *
 * @method \App\Model\Entity\CatFavoritosUsuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatFavoritosUsuario findOrCreate($search, callable $callback = null, $options = [])
 */
class CatFavoritosUsuarioTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_FAVORITOS_USUARIO');
        $this->setDisplayField('USUARIO_FK');
        $this->setPrimaryKey(['USUARIO_FK', 'REGISTRO_FK']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('USUARIO_FK')
            ->allowEmptyString('USUARIO_FK', 'create');

        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        return $validator;
    }
}
