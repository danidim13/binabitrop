<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatPMonogCap Model
 *
 * @method \App\Model\Entity\CatPMonogCap get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatPMonogCap newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatPMonogCap[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatPMonogCap|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatPMonogCap saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatPMonogCap patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatPMonogCap[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatPMonogCap findOrCreate($search, callable $callback = null, $options = [])
 */
class CatPMonogCapTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_P_MONOG_CAP');
        $this->setDisplayField('REGISTRO_FK');
        $this->setPrimaryKey('REGISTRO_FK');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        $validator
            ->scalar('COLACION_M_C')
            ->maxLength('COLACION_M_C', 1000)
            ->allowEmptyString('COLACION_M_C');

        $validator
            ->scalar('EDITORIAL')
            ->maxLength('EDITORIAL', 2000)
            ->allowEmptyString('EDITORIAL');

        $validator
            ->scalar('ISBN_S')
            ->maxLength('ISBN_S', 100)
            ->allowEmptyString('ISBN_S');

        $validator
            ->scalar('TITUL_PADR_M_C')
            ->maxLength('TITUL_PADR_M_C', 1000)
            ->allowEmptyString('TITUL_PADR_M_C');

        return $validator;
    }
}
