<?php
namespace App\Model\Table;

use App\Auth\BcryptAuthenticate;
use App\Model\Entity\SegUsuario;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * SegUsuario Model
 *
 * @method \App\Model\Entity\SegUsuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\SegUsuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SegUsuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SegUsuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SegUsuario saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SegUsuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SegUsuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SegUsuario findOrCreate($search, callable $callback = null, $options = [])
 */
class SegUsuarioTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('SEG_USUARIO');
        $this->setDisplayField('ID_USUARIO');
        $this->setPrimaryKey('ID_USUARIO');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID_USUARIO')
            ->allowEmptyString('ID_USUARIO', 'create');

        $validator
            ->scalar('NOMBRE_USUARIO')
            ->maxLength('NOMBRE_USUARIO', 30)
            ->requirePresence('NOMBRE_USUARIO', 'create')
            ->allowEmptyString('NOMBRE_USUARIO', false)
            ->add('NOMBRE_USUARIO', 'unique', ['rule' => 'validateUnique', 'message' => 'Username is already in use.' ,'provider' => 'table'])
            ->add('NOMBRE_USUARIO', 'length', ['rule' => ['minlength', 3], 'message' => 'Username must be at least three characters long.'])
            ->add('NOMBRE_USUARIO', 'length', ['rule' => ['maxlength', 30], 'message' => 'Username capacity exceeded.'])
            ->add('NOMBRE_USUARIO', 'validFormat',['rule' => array('custom', '/^([a-zA-Z])([a-zA-Z0-9\_\-\.]{2,29})/'), 'message' => 'Invalid username format, it must begin with a letter.']);

        $validator
            ->scalar('NOMBRE')
            ->maxLength('NOMBRE', 50)
            ->requirePresence('NOMBRE', 'create')
            ->allowEmptyString('NOMBRE', false)
            ->add('NOMBRE', 'validFormat',['rule' => array('custom', '/[a-zA-Z]+[ a-zA-Z]*/'), 'message' => 'Invalid name format.']);

        $validator
            ->scalar('PRIMER_APELLIDO')
            ->maxLength('PRIMER_APELLIDO', 50)
            ->requirePresence('PRIMER_APELLIDO', 'create')
            ->allowEmptyString('PRIMER_APELLIDO', false)
            ->add('PRIMER_APELLIDO', 'validFormat',['rule' => array('custom', '/[a-zA-Z]+[ a-zA-Z]*/'), 'message' => 'Invalid last name format.']);

        $validator
            ->scalar('CORREO_ELECTRONICO')
            ->maxLength('CORREO_ELECTRONICO', 100)
            ->requirePresence('CORREO_ELECTRONICO', 'create')
            ->allowEmptyString('CORREO_ELECTRONICO', false)
            ->add('CORREO_ELECTRONICO', 'unique', ['rule' => 'validateUnique', 'message' => 'E-mail provided is already in use.','provider' => 'table'])
            ->add('CORREO_ELECTRONICO', 'mustBeEmail', ['rule' => 'email', 'message' => 'Invalid e-mail format.']);

        $validator
            ->scalar('BCRYPT_HASH')
            ->maxLength('BCRYPT_HASH', 60);

        $validator
            ->scalar('TOKEN')
            ->maxLength('TOKEN', 7)
            ->allowEmptyString('TOKEN');

        $validator
            ->scalar('ACTIVO')
            ->maxLength('ACTIVO', 1)
            ->allowEmptyString('ACTIVO', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['NOMBRE_USUARIO']));

        return $rules;
    }

    /**
     * Handles validation for new user registration.
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @author Daniel Díaz
     * Changes:
     *          11/4/2019
     *          Added the comparison with the confirmation password.
     *          @author Adrian Alvarez
     */
    public function validationRegister(Validator $validator)
    {

        $validator = $this->validationDefault($validator);

        $validator
            ->scalar('password')
            ->allowEmptyString('password', false)
            ->requirePresence('password', true)
            ->add('password', 'length', [
                'rule' => ['minLength', 8],
                'message' => __('Passwords should be at least 8 characters long'),
            ])
            ->add('password', 'securePassword', [
                'rule' => 'isSecurePassword',
                'message' => __('Passwords should contain: 1 uppercase letter, 
                1 lowercase letter, and 1 special character (like "!.?_", etc...)'),
                'provider' => 'table'
            ]);

        $validator
            ->scalar('password_conf')
            ->allowEmptyString('password_conf', false)
            ->requirePresence('password',true)
            ->add('password','passwordMatch',[
                'rule' => 'comparePasswordWithConfirmation',
                'message' => __('Password doesn\'t match with the confirmation password.'),
                'provider' => 'table'
            ]);
        return $validator;
    }

    /**
     * Validación de formulario de login
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @author Daniel Díaz
     */
    public function validationLogin(Validator $validator)
    {
        $validator
            ->scalar('username')
            ->requirePresence('username', true)
            ->add('password', 'notEmpty', [
                'rule' => 'notEmpty',
                'message' => __('Please enter your username.')
            ]);

        $validator
            ->scalar('password')
            ->requirePresence('password', true)
            ->add('password', 'notEmpty', [
                'rule' => 'notEmpty',
                'message' => __('Please enter your password.')
            ]);


        return $validator;
    }

    /**
     * Verifies that the password entered and the password confirmation are the same.
     * @param string $password password to be checked.
     * @param string $password_conf confirmation that the user is typing the desired password.
     * @return bool true if both passwords match, false otherwise.
     * @author Adrian Alvarez
     */
    public function comparePasswordWithConfirmation($password, $password_conf){
        if($password === $password_conf['data']['password_conf'])return true;
        return false;
    }

    /**
     * Verifies that a password complies with security standards.
     * Passwords need to have:
     *   - 1 uppercase letter
     *   - 1 lowercase letter
     *   - 1 numeral
     *   - 1 special character
     * @param string $password Password to be checked.
     * @return bool Returns true if the password fullfills all requirementsm, false otherwise.
     * @author Daniel Díaz
     */
    public function isSecurePassword($password, array $context)
    {
        if (!preg_match('#[0-9]#', $password)) {
            return false;
        }

        if (!preg_match('#[A-Z]#', $password)) {
            return false;
        }

        if (!preg_match('#[a-z]#', $password)) {
            return false;
        }

        if (!preg_match('#\W|_#', $password)) {
            return false;
        }

        return true;
    }

    /**
     * Acciones que y controles antes de guardar/actualizar tuplas.
     * Este codigo se corre cuando se llama a $this->SegUsuario->save($entity)
     *
     * @return boolean Para continuar con la insersion retorna true (false interrumpe el proceso).
     * @author Daniel Díaz Molina
     *
     * Comentado el 16/4/19, la encriptación se hace en registrar usuario
     */
    /*
    public function beforeSave(Event $event)
    {
        $entity = $event->getData('entity');

        // debug($entity);

        if (!$this->encryptPassword($entity)) {
            debug('Error encriptando contrasena');
            debug($entity);
            return false;
        };

        // debug($entity);
        // die();

        return true;
    }
    */

    /**
     * Encriptar la contraseña con bcrypt de ser necesario.
     *
     * @param App\Model\Entity\SegUsuario $entity Instancia de la entidad usuario.
     * @return boolean Verdadero si se encriptó correctamente o no fue necesario, falso en caso de error.
     * @author Daniel Díaz Molina
     */
    private function encryptPassword(SegUsuario $entity)
    {
        // Make a password with bcrypt.
        if($entity->has('password')) {

            if (strlen($entity->password) < 8 || !$entity->has('NOMBRE_USUARIO')) {
                return false;
            }


            $entity->BCRYPT_HASH = BcryptAuthenticate::password($entity->NOMBRE_USUARIO, $entity->password);

            if ($entity->BCRYPT_HASH === false) {
                return false;
            }

            // debug($password_plain);
            // debug($cost);
            // debug('Setting password hash: ' . $entity->BCRYPT_HASH);
            // die();
        }
        return true;
    }

    /**
     * Registra al usuario en el sistema llamando al procedimiento almacenado
     *
     * @param $userToAdd Entidad del usuario que se va a agregar a la base de datos.
     * @return int Retorna el resultado de ingresar al usuario a la bd mediante el procedimiento almacenado
     *              Valores actuales: 1 => se inserto correctamente el usuario.
     *                               -1 => ya estaba el usuario en la tabla.
     * @author Adrian Alvarez
     *
     * @CHANGELOG
     *              @date: 5/8/2019
     *              @change: Cambio el binding de las variables para que funcione con el conector de oracle.
     *              @author: Adrián Álvarez.
     */
    public function registrarUsuario(SegUsuario $userToAdd){
        $insertionResult = 0; //Variable resutado del SP. Si es 1 el usuario se inserto correctamente.
        if (!$this->encryptPassword($userToAdd)) {
            return 0;
        };
        $method = \CakeDC\OracleDriver\ORM\MethodRegistry::get('userInsertion',['method' => 'SP_SEG_REG_USUARIO']);
        $nombre = $userToAdd['NOMBRE'];
        $apellidos = $userToAdd['PRIMER_APELLIDO'];
        $username = $userToAdd['NOMBRE_USUARIO'];
        $correo = $userToAdd['CORREO_ELECTRONICO'];
        $passwrd = $userToAdd['BCRYPT_HASH'];

        $request = $method->newRequest(['P_NOMBRE_USUARIO' => $username, 'P_NOMBRE_PERS' => $nombre,
            'P_PRIMER_APELLIDO' => $apellidos, 'P_CORREO' => $correo, 'BCRYPT_HASH' => $passwrd,
            'RST_STATUS' => $insertionResult]);
        $method->execute($request);
        return $request['RST_STATUS'];
    }


    /**
     * Handles validation for password change.
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @author André Flasterstein
     */
    public function validationPassword(Validator $validator)
    {
        $validator
            ->scalar('password')
            ->allowEmptyString('password', false)
            ->requirePresence('password', true)
            ->add('password', 'length', [
                'rule' => ['minLength', 8],
                'message' => __('Passwords should be at least 8 characters long'),
            ])
            ->add('password', 'securePassword', [
                'rule' => 'isSecurePassword',
                'message' => __('Passwords should contain: 1 uppercase letter, 1 lowercase letter, and 1 special character (like "!.?_", etc...)'),
                'provider' => 'table'
            ]);

        $validator
            ->scalar('password_conf')
            ->allowEmptyString('password_conf', false)
            ->requirePresence('password',true)
            ->add('password','passwordMatch',[
                'rule' => 'comparePasswordWithConfirmation',
                'message' => __('Password doesn\'t match with the confirmation password.'),
                'provider' => 'table'
            ]);
        return $validator;
    }

    /**
     * Registra al usuario en el sistema llamando al procedimiento almacenado
     *
     * @param $userToAdd Entidad del usuario que va a cambiar la constrasena.
     * @return int Retorna el resultado de cambiar la contrasena
     * @author André Flasterstein
     */
    public function changePassword(SegUsuario $userToAdd){
        if (!$this->encryptPassword($userToAdd)) {
            return 0;
        };
        $users = TableRegistry::get('SegUsuario');

        $query = $users->query();
        $query->update()
            ->set(['BCRYPT_HASH' => $userToAdd->BCRYPT_HASH])
            ->where(['ID_USUARIO' => $userToAdd->ID_USUARIO])
            ->execute();
        return 1;
    }
}
