<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatSeriada Model
 *
 * @method \App\Model\Entity\CatSeriada get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatSeriada newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatSeriada[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatSeriada|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatSeriada saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatSeriada patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatSeriada[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatSeriada findOrCreate($search, callable $callback = null, $options = [])
 */
class CatSeriadaTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_SERIADA');
        $this->setDisplayField('REGISTRO_FK');
        $this->setPrimaryKey('REGISTRO_FK');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        $validator
            ->scalar('TITULO_SERIADA2')
            ->maxLength('TITULO_SERIADA2', 2000)
            ->allowEmptyString('TITULO_SERIADA2');

        $validator
            ->scalar('ISSN')
            ->maxLength('ISSN', 100)
            ->allowEmptyString('ISSN');

        $validator
            ->scalar('MENC_DE_LAS_PART')
            ->maxLength('MENC_DE_LAS_PART', 2000)
            ->allowEmptyString('MENC_DE_LAS_PART');

        return $validator;
    }
}
