<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatTesis Model
 *
 * @method \App\Model\Entity\CatTesi get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatTesi newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatTesi[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatTesi|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatTesi saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatTesi patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatTesi[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatTesi findOrCreate($search, callable $callback = null, $options = [])
 */
class CatTesisTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_TESIS');
        $this->setDisplayField('REGISTRO_FK');
        $this->setPrimaryKey('REGISTRO_FK');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        $validator
            ->scalar('COLACION_M_C')
            ->maxLength('COLACION_M_C', 1000)
            ->allowEmptyString('COLACION_M_C');

        $validator
            ->scalar('EDITORIAL')
            ->maxLength('EDITORIAL', 2000)
            ->allowEmptyString('EDITORIAL');

        $validator
            ->scalar('TESIS')
            ->maxLength('TESIS', 1000)
            ->allowEmptyString('TESIS');

        return $validator;
    }
}
