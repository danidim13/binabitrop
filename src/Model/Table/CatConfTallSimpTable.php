<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatConfTallSimp Model
 *
 * @method \App\Model\Entity\CatConfTallSimp get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatConfTallSimp newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatConfTallSimp[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatConfTallSimp|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatConfTallSimp saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatConfTallSimp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatConfTallSimp[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatConfTallSimp findOrCreate($search, callable $callback = null, $options = [])
 */
class CatConfTallSimpTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('CAT_CONF_TALL_SIMP');
        $this->setDisplayField('REGISTRO_FK');
        $this->setPrimaryKey('REGISTRO_FK');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('REGISTRO_FK')
            ->allowEmptyString('REGISTRO_FK', 'create');

        $validator
            ->scalar('COLACION_M_C')
            ->maxLength('COLACION_M_C', 1000)
            ->allowEmptyString('COLACION_M_C');

        $validator
            ->scalar('REUNION_ES')
            ->maxLength('REUNION_ES', 1000)
            ->allowEmptyString('REUNION_ES');

        return $validator;
    }
}
