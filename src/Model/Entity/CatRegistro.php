<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatRegistro Entity
 *
 * @property int $ID_REGISTRO
 * @property string|null $DISPONIBILIDAD
 * @property string|null $ACRON_CENTRO_PARTI
 * @property string|null $AFILIACION
 * @property string|null $AUTOR_INST_DOC_PADRE
 * @property string|null $AUTOR_INSTIT
 * @property string|null $AUTOR_PERSONAL_DOC_PADRE
 * @property string|null $AUTORES_INSTIT
 * @property string|null $AUTORES_PERSONAL
 * @property string|null $DESCRIPT_GEOG
 * @property string|null $DESCRIPT_LOCALES
 * @property string|null $DESCRIPT_PRIMAR
 * @property string|null $DESCRIPT_SECUND
 * @property int $DOCUMENTALISTA_S
 * @property string|null $FECHA_INGRESO_REGISTRO
 * @property string|null $FECHA_PUBL_EDICION_FROM_ISO
 * @property string|null $FECHA_PUBL_IN_EXT
 * @property string|null $IDREG
 * @property string|null $ISSN2
 * @property string|null $MENC_DE_LAS_PART
 * @property string|null $N_REGISTROS_DE_LAS_PARTES
 * @property string|null $RESUMEN
 * @property string|null $SIGNATURA_TOPOGRAFICA
 * @property string|null $TIPO_DE_MATERIAL
 * @property string|null $TITULO
 * @property string|null $UBICACION
 * @property string|null $NIVEL_BIBLIOGR_DOC_PADRE
 * @property string|null $NIVEL_BIBLIOGRAFICO
 * @property string|null $NOTAS
 * @property string|null $URL_OTS
 * @property string|null $PDF
 * @property string $ACTIVO
 *
 * @property \App\Model\Entity\CatConfTallerSimposio $cat_conf_taller_simposio
 * @property \App\Model\Entity\CatMonogLibro $cat_monog_libro
 * @property \App\Model\Entity\CatPMonogCapLib $cat_p_monog_cap_lib
 * @property \App\Model\Entity\CatSeriada $cat_seriada
 * @property \App\Model\Entity\CatTesi $cat_tesi
 */
class CatRegistro extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'DISPONIBILIDAD' => true,
        'ACRON_CENTRO_PARTI' => true,
        'AFILIACION' => true,
        'AUTOR_INST_DOC_PADRE' => true,
        'AUTOR_INSTIT' => true,
        'AUTOR_PERSONAL_DOC_PADRE' => true,
        'AUTORES_INSTIT' => true,
        'AUTORES_PERSONAL' => true,
        'DESCRIPT_GEOG' => true,
        'DESCRIPT_LOCALES' => true,
        'DESCRIPT_PRIMAR' => true,
        'DESCRIPT_SECUND' => true,
        'DOCUMENTALISTA_S' => true,
        'FECHA_INGRESO_REGISTRO' => true,
        'FECHA_PUBL_EDICION_FROM_ISO' => true,
        'FECHA_PUBL_IN_EXT' => true,
        'IDREG' => true,
        'ISSN2' => true,
        'MENC_DE_LAS_PART' => true,
        'N_REGISTROS_DE_LAS_PARTES' => true,
        'RESUMEN' => true,
        'SIGNATURA_TOPOGRAFICA' => true,
        'TIPO_DE_MATERIAL' => true,
        'TITULO' => true,
        'UBICACION' => true,
        'NIVEL_BIBLIOGR_DOC_PADRE' => true,
        'NIVEL_BIBLIOGRAFICO' => true,
        'NOTAS' => true,
        'URL_OTS' => true,
        'PDF' => true,
        'ACTIVO' => true,
        'cat_conf_taller_simposio' => true,
        'cat_monog_libro' => true,
        'cat_p_monog_cap_lib' => true,
        'cat_seriada' => true,
        'cat_tesi' => true
    ];
}
