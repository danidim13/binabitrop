<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatSeriada Entity
 *
 * @property int $REGISTRO_FK
 * @property string|null $TITULO_SERIADA2
 * @property string|null $ISSN
 * @property string|null $MENC_DE_LAS_PART
 */
class CatSeriada extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'TITULO_SERIADA2' => true,
        'ISSN' => true,
        'MENC_DE_LAS_PART' => true
    ];
}
