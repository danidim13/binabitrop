<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatConfTallSimp Entity
 *
 * @property int $REGISTRO_FK
 * @property string|null $COLACION_M_C
 * @property string|null $REUNION_ES
 */
class CatConfTallSimp extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'COLACION_M_C' => true,
        'REUNION_ES' => true
    ];
}
