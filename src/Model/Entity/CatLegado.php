<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatLegado Entity
 *
 * @property int $REGISTRO_FK
 * @property string|null $FECHA_INGRESO
 * @property string $ISO_PAIS_ORIG
 * @property string|null $FECHA_PUBLICACION
 * @property string|null $ESTADO_PROCESO
 * @property string|null $TIPO_ADQUISICION
 * @property string|null $SUPLIDOR
 * @property string|null $FECHA_REGISTRO_DE_AUTORIDADES
 * @property string|null $FECHA_MODIFICACION_DEL_REG
 * @property string|null $SOLICTANTE_S
 * @property string|null $REFERENCIA_S_DE_VEASE
 * @property string|null $REFERENCIA_S_DE_VEASE2
 * @property string|null $TITULO_PARALELO_ING
 * @property string|null $TITULO_PARALELO_ESP
 * @property string|null $TITULO_PARALELO_FRN
 * @property string|null $TITULOS_PARALELOS
 * @property string|null $TITULO_S_ANTERIOR_ES
 * @property string|null $TITULO_S_POSTERIOR_ES
 * @property string|null $TITULO_PARALELO_OTRO_IDIOMA
 * @property string|null $VER_TMB_EDIC_EN_OTRO_IDIOMA
 * @property string|null $VERSIONES_EN_OTROS_IDIOMAS
 * @property string|null $PROYECTO_S
 * @property string|null $PRECIO
 * @property string|null $OTRAS_INSTITU_ASOC
 * @property string|null $NUMERO_S_DE_ACCESO
 * @property string|null $NUMERO_DE_COPIAS
 * @property string|null $NUMERO_COPIAS_ORDENADAS
 * @property string|null $NUMERO_REGISTRO_DE_AUTORI
 * @property string|null $NOTAS_ADQUISICION
 * @property string|null $EDICION
 * @property string|null $ESTADO_SOLICITUD
 * @property string|null $N_S_DOCUMENTOS_S
 * @property string|null $NOMBRE_Y_DIR_SUPLIDOR
 * @property string|null $NOMBRE_S_PORTERIOR_ES
 * @property string|null $NOMBRE_S_ANTERIOR_ES
 * @property string|null $FECHA_DEL_RECLAMO
 * @property string|null $ESTADO_PROCESAMIENTO
 * @property string|null $IDIOMA_DEL_ANALISIS
 * @property string|null $N_REG_CENTRO_PARTICIPANTE
 * @property string|null $N_REG_VERSION_OTROS_IDIOMAS
 * @property string|null $ENCABEZ_TEMATICO_GEN_
 * @property string|null $ISBN
 * @property string|null $NIVEL_BIBLIOG
 * @property string|null $NIVEL_BIBLIO_DOC_PADRE
 * @property int|null $ID_BASE
 */
class CatLegado extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'FECHA_INGRESO' => true,
        'ISO_PAIS_ORIG' => true,
        'FECHA_PUBLICACION' => true,
        'ESTADO_PROCESO' => true,
        'TIPO_ADQUISICION' => true,
        'SUPLIDOR' => true,
        'FECHA_REGISTRO_DE_AUTORIDADES' => true,
        'FECHA_MODIFICACION_DEL_REG' => true,
        'SOLICTANTE_S' => true,
        'REFERENCIA_S_DE_VEASE' => true,
        'REFERENCIA_S_DE_VEASE2' => true,
        'TITULO_PARALELO_ING' => true,
        'TITULO_PARALELO_ESP' => true,
        'TITULO_PARALELO_FRN' => true,
        'TITULOS_PARALELOS' => true,
        'TITULO_S_ANTERIOR_ES' => true,
        'TITULO_S_POSTERIOR_ES' => true,
        'TITULO_PARALELO_OTRO_IDIOMA' => true,
        'VER_TMB_EDIC_EN_OTRO_IDIOMA' => true,
        'VERSIONES_EN_OTROS_IDIOMAS' => true,
        'PROYECTO_S' => true,
        'PRECIO' => true,
        'OTRAS_INSTITU_ASOC' => true,
        'NUMERO_S_DE_ACCESO' => true,
        'NUMERO_DE_COPIAS' => true,
        'NUMERO_COPIAS_ORDENADAS' => true,
        'NUMERO_REGISTRO_DE_AUTORI' => true,
        'NOTAS_ADQUISICION' => true,
        'EDICION' => true,
        'ESTADO_SOLICITUD' => true,
        'N_S_DOCUMENTOS_S' => true,
        'NOMBRE_Y_DIR_SUPLIDOR' => true,
        'NOMBRE_S_PORTERIOR_ES' => true,
        'NOMBRE_S_ANTERIOR_ES' => true,
        'FECHA_DEL_RECLAMO' => true,
        'ESTADO_PROCESAMIENTO' => true,
        'IDIOMA_DEL_ANALISIS' => true,
        'N_REG_CENTRO_PARTICIPANTE' => true,
        'N_REG_VERSION_OTROS_IDIOMAS' => true,
        'ENCABEZ_TEMATICO_GEN_' => true,
        'ISBN' => true,
        'NIVEL_BIBLIOG' => true,
        'NIVEL_BIBLIO_DOC_PADRE' => true,
        'ID_BASE' => true
    ];
}
