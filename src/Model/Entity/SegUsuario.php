<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SegUsuario Entity
 *
 * @property int $ID_USUARIO
 * @property string $NOMBRE_USUARIO
 * @property string $ROL_FK
 * @property string $NOMBRE
 * @property string $PRIMER_APELLIDO
 * @property string $CORREO_ELECTRONICO
 * @property string $BCRYPT_HASH
 * @property string|null $TOKEN
 * @property string $ACTIVO
 */
class SegUsuario extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'NOMBRE_USUARIO' => true,
        'ROL_FK' => true,
        'NOMBRE' => true,
        'PRIMER_APELLIDO' => true,
        'CORREO_ELECTRONICO' => true,
        'BCRYPT_HASH' => true,
        'TOKEN' => true,
        'ACTIVO' => true,
        'BCRYPT_HASH' => false,
        '*' => false,
    ];
}
