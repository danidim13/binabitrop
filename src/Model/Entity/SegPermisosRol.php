<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SegPermisosRol Entity
 *
 * @property int $PERMISOS_ROL
 * @property string $ROL_FK
 * @property int $PERMISOS_FK
 */
class SegPermisosRol extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ROL_FK' => true,
        'PERMISOS_FK' => true
    ];
}
