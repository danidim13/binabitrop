<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;

/**
 * SegUsuario Controller
 *
 * @property \App\Model\Table\SegUsuarioTable $SegUsuario
 *
 * @method \App\Model\Entity\SegUsuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SegUsuarioController extends AppController
{

    public $components = array("Email");

    /**
     * Aplicar highlight al menú de navegación
     * @param Cake\Event\Event $event Event instance.
     * @author Daniel Díaz
     *  @change-log
     *              @date 22 Abril 2019
     *              @change Agrego el cambio de seguridad para aceptar el cambio de link en el modal del delete del index de usuario.
     *              @author Adrian Alvarez
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        if(isset($this->viewVars['navbarLinks']['users'])) {
            $this->viewVars['navbarLinks']['users']['active'] = true;
        }

        $this->Security->setConfig('unlockedActions',['delete']);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $segUsuario = $this->paginate($this->SegUsuario);

        $this->set(compact('segUsuario'));
    }

    /**
     * View method
     *
     * @param string|null $id Seg Usuario id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $segUsuario = $this->SegUsuario->get($id, [
            'contain' => []
        ]);

        $this->set('segUsuario', $segUsuario);
    }

    /**
     * Si los datos del usuario son validos, lo registra en el sistema, en caso contrario marca los datos invalidos
     * para que sean indicados en la vista.
     *
     * @author Adrian Alvarez
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $segUsuario = $this->SegUsuario->newEntity();

        if ($this->request->is('post')) {
            $segUsuario = $this->SegUsuario->patchEntity($segUsuario, $this->request->getData(), [
                'validate' => 'register',
                'accessibleFields' => [
                    'NOMBRE_USUARIO' => true,
                    'password' => true
                ]
            ]);
            if ($segUsuario->hasErrors() == false) {
                if ($this->SegUsuario->registrarUsuario($segUsuario) == 1) {
                    $this->Flash->success(__('Sign up successful, a confirmation email was sent to ' . $segUsuario->CORREO_ELECTRONICO .'.'));
                    $this->notifyUserRegistry($segUsuario);
                    return $this->redirect('/');
                }
            }
            $this->Flash->error(__('The user could not be registered. Please, try again.'));
        }
        $this->set(compact('segUsuario'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Seg Usuario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $segUsuario = $this->SegUsuario->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $segUsuario = $this->SegUsuario->patchEntity($segUsuario, $this->request->getData());
            if ($this->SegUsuario->save($segUsuario)) {
                $this->Flash->success(__('The seg usuario has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg usuario could not be saved. Please, try again.'));
        }
        $this->set(compact('segUsuario'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Seg Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $segUsuario = $this->SegUsuario->get($id);
        if ($this->SegUsuario->delete($segUsuario)) {
            $this->Flash->success(__('The seg usuario has been deleted.'));
            if($segUsuario->ID_USUARIO === $this->Auth->user('ID_USUARIO')){
                return $this->logout();
            }
        } else {
            $this->Flash->error(__('The seg usuario could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Login
     * @author Daniel Díaz
     * @return \Cake\Http\Response|null Redirige a la página que se solicitó si se loggeó correctamente
     */
    public function login()
    {
        $segUsuario = $this->SegUsuario->newEntity();
        if ($this->request->is('post')) {

            $segUsuario = $this->SegUsuario->patchEntity($segUsuario, $this->request->getData(), [
                'validate' => 'login',
                'accessibleFields' => [
                    'username' => true,
                    'password' => true,
                    '*' => false
                ]
            ]);

            if (!$segUsuario->errors()) {
                $user = $this->Auth->identify();
    
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('Username or password is incorrect'));
                }
            }
        }
        $this->set(compact('segUsuario'));
    }

    /**
     * Logout method
     * @author Daniel Díaz
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }   
    
    /**
     * Change Password
     * @author André Flasterstein
     */
    public function changePassword()
    {
        $segUsuario = $this->SegUsuario->newEntity();
        $segUsuario->ID_USUARIO = $this->Auth->user('ID_USUARIO');
        $segUsuario->NOMBRE_USUARIO = $this->Auth->user('NOMBRE_USUARIO');

        if ($this->request->is('post')) {
            $segUsuario = $this->SegUsuario->patchEntity($segUsuario, $this->request->getData(), [
                'validate' => 'password',
                'accessibleFields' => [
                    'password' => true
                ]
            ]);
            if ($segUsuario->hasErrors() == false) {
                if ($this->SegUsuario->changePassword($segUsuario) == 1) {
                    $this->Flash->success(__('The new password has been saved.'));
                    return $this->redirect(['action' => 'view', $this->Auth->user('ID_USUARIO')]);
                }
            }
            $this->Flash->error(__('The new password could not be saved. Please, try again.'));
        }
        $this->set(compact('segUsuario'));
    }

    /**
     * Metodo para notificar al usuario por correo electronico que ha sido registrado en el 
     * sistema.
     * @author Adrian Alvarez
     */
    public function notifyUserRegistry($segUsuario){
        $message = "Dear user, your account for BINABITROP has been created.";
        $subject = "BINABITROP account created.";
        try{
            $this->Email->send_mail($segUsuario['CORREO_ELECTRONICO'], $subject, $message);
        }catch(Exception $e){
            //debug("Error sending the message. Error:", $this->Email->ErrorInfo);
        }
    }

}
