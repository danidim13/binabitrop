<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\App;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require ROOT. '/vendor/phpmailer/phpmailer/src/Exception.php';
require ROOT. '/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require ROOT. '/vendor/phpmailer/phpmailer/src/SMTP.php';

/**
 * Componente para enviar correos de notificacion a los usuarios del sistema.
 * 
 * Codigo obtenido del siguiente video: https://www.youtube.com/watch?v=bXdN2eU1b3k
 * @author Adrian Alvarez.
 */
class EmailComponent extends Component {
    
    public function send_mail($to, $subject, $message)
    {
        $sender = "binabitroptestemail@gmail.com";
        $header = "X-Mailer: PHP/".phpversion() . "Return-Path: $sender";
        $mail = new PHPMailer();
        $mail->SMTPDebug  = 2; // turn it off in production = 0
        $mail->IsSMTP();
        $mail->Host = "smtp.gmail.com"; 
        $mail->SMTPAuth = true;
        $mail->Username   = "binabitroptestemail@gmail.com";  
        $mail->Password   = "ingenieria123";
        $mail->SMTPSecure = "tls"; // ssl and tls
        $mail->Port = 587; // 465 and 587
        $mail->SMTPOptions = array (
            'tls' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ),
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
            
        $mail->From = $sender;
        $mail->FromName = "From Me";
        $mail->AddAddress($to);
        $mail->IsHTML(true);
        $mail->CreateHeader($header);
        $mail->Subject = $subject;
        $mail->Body    = nl2br($message);
        $mail->AltBody = nl2br($message);
        // return an array with two keys: error & message
        if(!$mail->Send()) {
            return array('error' => true, 'message' => 'Mailer Error: ' . $mail->ErrorInfo);
        } else {
            return array('error' => false, 'message' =>  "Message sent!");
        }
    }
}