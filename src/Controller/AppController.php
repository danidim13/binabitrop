<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Controller\Exception\SecurityException;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     * @author Daniel Díaz:
     *      * Auth 16/4/19
     *      * Security 16/4/19
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        
        $this->loadComponent('Auth', [
            'storage' => 'Session',
            'loginAction' => [
                'controller' => 'SegUsuario',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'CatRegistro',
                'action' => 'search',
            ],
            'logoutRedirect' => [
                'controller' => 'CatRegistro',
                'action' => 'search',
            ],
            'authenticate' => [
                'Bcrypt' => [
                    'userModel' => 'SegUsuario',
                    'fields' => ['username' => 'NOMBRE_USUARIO', 'password' => 'BCRYPT_HASH'],
                    'realm' => 'BINABITROP'
                ]
            ],
        ]);
        // Permitir todas las acciones sin login por default
        $this->Auth->allow();

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security', [
            'blackHoleCallback' => 'handleBlackHole'
        ]);
        
        /**
         * Forzar conexión segura en toda la aplicación
         * NOTE: comentar esta línea para poder usar http y evitar problemas con el certificado
         */
        $this->Security->requireSecure();

    }

    /**
     * Este método se llama si alguna acción viola la configuración de seguridad.
     * Si la violación es por un acceso inseguro se redirige a https, de lo contrario
     * se vuelve a levantar la excepción.
     * @param string $type Tipo de violación ('secure' o 'auth').
     * @param Cake\Controller\Exception\SecurityException $exception
     * @return Cake\Http\Response|null
     * @throws Cake\Controller\Exception\SecurityException
     * @author Daniel Díaz
     */
    public function handleBlackHole($type, SecurityException $exception)
    {
        if ($type === 'secure') {
            return $this->forceSSL();
        }

        throw $exception;
    }

    /**
     * Redigir a url con https
     * @return Cake\Http\Response|null
     * @author Daniel Díaz
     */
    public function forceSSL()
    {
        //debug($this->request->getRequestTarget());
        return $this->redirect('https://' . env('SERVER_NAME') . Router::url($this->request->getRequestTarget()));
    }

    /**
     * Lógica relacionada al usuario loggeado.
     * 
     * Se setean variables para la vista con información de las acciones en el navbar y el usuario loggeado
     * @param Cake\Event\Event $event Event instance.
     * @author Daniel Díaz
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $navbarLinks = [
            'search' => ['text' => __('Search'), 'link' => ['controller' => 'CatRegistro', 'action' => 'search'], 'active' => false],
        ];

        $logged = $this->Auth->user();
        if ($logged) {
            $loggedInfo = [
                'ID_USUARIO' => $logged->ID_USUARIO,
                'NOMBRE_USUARIO' => $logged->NOMBRE_USUARIO,
                'NOMBRE' => $logged->NOMBRE,
                'PRIMER_APELLIDO' => $logged->PRIMER_APELLIDO,
            ];
            $this->set('loggedUser', $loggedInfo);

            // TODO: Actualizar link
            $navbarLinks['favorites'] = ['text' => __('Favorites'), 'link' => '#', 'active' => false];

            /**
             * TODO:
             * Definir los roles adicionales y las acciones que van a poder ver en la 
             * barra de navegación.
             * Actuliazar link de My Entries
             */
            if ($logged->ROL_FK === 'Admin') {
                $navbarLinks['entries'] = ['text' => __('My Entries'), 'link' => '#', 'active' => false];
            }

            if ($logged->ROL_FK === 'root') {
                $navbarLinks['entries'] = ['text' => __('My Entries'), 'link' => '#', 'active' => false];
                $navbarLinks['users'] = ['text' => __('Users'), 'link' => ['controller' => 'SegUsuario', 'action' => 'index'], 'active' => false];
            }

        }
        $this->set('navbarLinks', $navbarLinks);
    }

}
