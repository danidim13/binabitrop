<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CatFavoritosUsuario Controller
 *
 * @property \App\Model\Table\CatFavoritosUsuarioTable $CatFavoritosUsuario
 *
 * @method \App\Model\Entity\CatFavoritosUsuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CatFavoritosUsuarioController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $catFavoritosUsuario = $this->paginate($this->CatFavoritosUsuario);

        $this->set(compact('catFavoritosUsuario'));
    }

    /**
     * View method
     *
     * @param string|null $id Cat Favoritos Usuario id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catFavoritosUsuario = $this->CatFavoritosUsuario->get($id, [
            'contain' => []
        ]);

        $this->set('catFavoritosUsuario', $catFavoritosUsuario);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catFavoritosUsuario = $this->CatFavoritosUsuario->newEntity();
        if ($this->request->is('post')) {
            $catFavoritosUsuario = $this->CatFavoritosUsuario->patchEntity($catFavoritosUsuario, $this->request->getData());
            if ($this->CatFavoritosUsuario->save($catFavoritosUsuario)) {
                $this->Flash->success(__('The cat favoritos usuario has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cat favoritos usuario could not be saved. Please, try again.'));
        }
        $this->set(compact('catFavoritosUsuario'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cat Favoritos Usuario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catFavoritosUsuario = $this->CatFavoritosUsuario->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catFavoritosUsuario = $this->CatFavoritosUsuario->patchEntity($catFavoritosUsuario, $this->request->getData());
            if ($this->CatFavoritosUsuario->save($catFavoritosUsuario)) {
                $this->Flash->success(__('The cat favoritos usuario has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cat favoritos usuario could not be saved. Please, try again.'));
        }
        $this->set(compact('catFavoritosUsuario'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cat Favoritos Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catFavoritosUsuario = $this->CatFavoritosUsuario->get($id);
        if ($this->CatFavoritosUsuario->delete($catFavoritosUsuario)) {
            $this->Flash->success(__('The cat favoritos usuario has been deleted.'));
        } else {
            $this->Flash->error(__('The cat favoritos usuario could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
