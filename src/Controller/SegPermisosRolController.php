<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SegPermisosRol Controller
 *
 * @property \App\Model\Table\SegPermisosRolTable $SegPermisosRol
 *
 * @method \App\Model\Entity\SegPermisosRol[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SegPermisosRolController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $segPermisosRol = $this->paginate($this->SegPermisosRol);

        $this->set(compact('segPermisosRol'));
    }

    /**
     * View method
     *
     * @param string|null $id Seg Permisos Rol id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $segPermisosRol = $this->SegPermisosRol->get($id, [
            'contain' => []
        ]);

        $this->set('segPermisosRol', $segPermisosRol);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $segPermisosRol = $this->SegPermisosRol->newEntity();
        if ($this->request->is('post')) {
            $segPermisosRol = $this->SegPermisosRol->patchEntity($segPermisosRol, $this->request->getData());
            if ($this->SegPermisosRol->save($segPermisosRol)) {
                $this->Flash->success(__('The seg permisos rol has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg permisos rol could not be saved. Please, try again.'));
        }
        $this->set(compact('segPermisosRol'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Seg Permisos Rol id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $segPermisosRol = $this->SegPermisosRol->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $segPermisosRol = $this->SegPermisosRol->patchEntity($segPermisosRol, $this->request->getData());
            if ($this->SegPermisosRol->save($segPermisosRol)) {
                $this->Flash->success(__('The seg permisos rol has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg permisos rol could not be saved. Please, try again.'));
        }
        $this->set(compact('segPermisosRol'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Seg Permisos Rol id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $segPermisosRol = $this->SegPermisosRol->get($id);
        if ($this->SegPermisosRol->delete($segPermisosRol)) {
            $this->Flash->success(__('The seg permisos rol has been deleted.'));
        } else {
            $this->Flash->error(__('The seg permisos rol could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
