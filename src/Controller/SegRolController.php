<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SegRol Controller
 *
 * @property \App\Model\Table\SegRolTable $SegRol
 *
 * @method \App\Model\Entity\SegRol[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SegRolController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $segRol = $this->paginate($this->SegRol);

        $this->set(compact('segRol'));
    }

    /**
     * View method
     *
     * @param string|null $id Seg Rol id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $segRol = $this->SegRol->get($id, [
            'contain' => []
        ]);

        $this->set('segRol', $segRol);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $segRol = $this->SegRol->newEntity();
        if ($this->request->is('post')) {
            $segRol = $this->SegRol->patchEntity($segRol, $this->request->getData());
            if ($this->SegRol->save($segRol)) {
                $this->Flash->success(__('The seg rol has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg rol could not be saved. Please, try again.'));
        }
        $this->set(compact('segRol'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Seg Rol id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $segRol = $this->SegRol->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $segRol = $this->SegRol->patchEntity($segRol, $this->request->getData());
            if ($this->SegRol->save($segRol)) {
                $this->Flash->success(__('The seg rol has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg rol could not be saved. Please, try again.'));
        }
        $this->set(compact('segRol'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Seg Rol id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $segRol = $this->SegRol->get($id);
        if ($this->SegRol->delete($segRol)) {
            $this->Flash->success(__('The seg rol has been deleted.'));
        } else {
            $this->Flash->error(__('The seg rol could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
