<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SegPermiso Controller
 *
 * @property \App\Model\Table\SegPermisoTable $SegPermiso
 *
 * @method \App\Model\Entity\SegPermiso[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SegPermisoController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $segPermiso = $this->paginate($this->SegPermiso);

        $this->set(compact('segPermiso'));
    }

    /**
     * View method
     *
     * @param string|null $id Seg Permiso id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $segPermiso = $this->SegPermiso->get($id, [
            'contain' => []
        ]);

        $this->set('segPermiso', $segPermiso);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $segPermiso = $this->SegPermiso->newEntity();
        if ($this->request->is('post')) {
            $segPermiso = $this->SegPermiso->patchEntity($segPermiso, $this->request->getData());
            if ($this->SegPermiso->save($segPermiso)) {
                $this->Flash->success(__('The seg permiso has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg permiso could not be saved. Please, try again.'));
        }
        $this->set(compact('segPermiso'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Seg Permiso id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $segPermiso = $this->SegPermiso->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $segPermiso = $this->SegPermiso->patchEntity($segPermiso, $this->request->getData());
            if ($this->SegPermiso->save($segPermiso)) {
                $this->Flash->success(__('The seg permiso has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The seg permiso could not be saved. Please, try again.'));
        }
        $this->set(compact('segPermiso'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Seg Permiso id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $segPermiso = $this->SegPermiso->get($id);
        if ($this->SegPermiso->delete($segPermiso)) {
            $this->Flash->success(__('The seg permiso has been deleted.'));
        } else {
            $this->Flash->error(__('The seg permiso could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
