<?php
namespace App\Controller;
use Cake\Event\Event;

use App\Controller\AppController;

/**
 * CatRegistro Controller
 *
 * @property \App\Model\Table\CatRegistroTable $CatRegistro
 *
 * @method \App\Model\Entity\CatRegistro[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CatRegistroController extends AppController
{

    /**
     * @var array required to paginate
     *
     * @author André Flasterstein
     */
    public $paginate = [
        'fields' => ['CatRegistro.ID_REGISTRO', 'CatRegistro.TIPO_DE_MATERIAL', 'CatRegistro.TITULO', 'CatRegistro.AUTORES_PERSONAL', 'CatRegistro.DISPONIBILIDAD'],
        'limit' => 10,
        'order' => [
            'CatRegistro.TITULO' => 'asc'
        ]
    ];

    /**
     * Initializes paginator component for custom pagination
     *
     * @author André Flasterstein
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    /**
     * Aplicar highlight al menú de navegación
     * @param Cake\Event\Event $event Event instance.
     * @author Daniel Díaz
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        if(isset($this->viewVars['navbarLinks']['search'])) {
            $this->viewVars['navbarLinks']['search']['active'] = true;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $catRegistro = $this->paginate($this->CatRegistro);

        $this->set(compact('catRegistro'));
    }

    /**
     * View method
     *
     * @author Fabian Alvarez
     * 
     * @param string|null $id Cat Registro id.
     * @param string|null $tipo_de_material Tipo de registro.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $tipo_de_material = null)
    {
        $catRegistro = $this->CatRegistro->consultRegister($id);
        /**
         * Pregunta que tipo de reigstro es y realiza un query dependiendo del tipo de registro específico
         */
        if ($tipo_de_material === 'PER') {
            $this->loadModel('CatSeriada');
            $catEspecializacion = $this->CatSeriada->get($id, [
                'contain' => []
            ]);
        } elseif ($tipo_de_material === 'CONF') {
            $this->loadModel('CatConfTallSimp');
            $catEspecializacion = $this->CatConfTallSimp->get($id, [
                'contain' => []
            ]);
        } elseif ($tipo_de_material === 'MONO') {
            $this->loadModel('CatMonogLibro');
            $catEspecializacion = $this->CatMonogLibro->get($id, [
                'contain' => []
            ]);
        } elseif ($tipo_de_material === 'PART') {
            $this->loadModel('CatPMonogCap');
            $catEspecializacion = $this->CatPMonogCap->get($id, [
                'contain' => []
            ]);
        } elseif ($tipo_de_material === 'THES') {
            $this->loadModel('CatTesis');
            $catEspecializacion = $this->CatTesis->get($id, [
                'contain' => []
            ]);
        }

        $this->set('catRegistro', $catRegistro);
        $this->set('catEspecializacion',$catEspecializacion);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catRegistro = $this->CatRegistro->newEntity();
        if ($this->request->is('post')) {
            $catRegistro = $this->CatRegistro->patchEntity($catRegistro, $this->request->getData());
            if ($this->CatRegistro->save($catRegistro)) {
                $this->Flash->success(__('The cat registro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cat registro could not be saved. Please, try again.'));
        }
        $this->set(compact('catRegistro'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cat Registro id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catRegistro = $this->CatRegistro->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catRegistro = $this->CatRegistro->patchEntity($catRegistro, $this->request->getData());
            if ($this->CatRegistro->save($catRegistro)) {
                $this->Flash->success(__('The cat registro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cat registro could not be saved. Please, try again.'));
        }
        $this->set(compact('catRegistro'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cat Registro id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catRegistro = $this->CatRegistro->get($id);
        if ($this->CatRegistro->delete($catRegistro)) {
            $this->Flash->success(__('The cat registro has been deleted.'));
        } else {
            $this->Flash->error(__('The cat registro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Search view method
     *
     * @return \Cake\Http\Response|void
     * @author André Flasterstein
     */
    public function search()
    {
        $catRegistro = $this->paginate();
        $this->set(compact('catRegistro'));
    }

    /**
     * Lookup view method, used to render the results in a table which is then loaded in the Search view
     * @return \Cake\Http\Response|void
     * @author André Flasterstein
     */
    public function lookup()
    {
        if ($this->request->is('get')) {
            $this->request->allowMethod('ajax');
            $keyword = $this->request->getQuery('keyword');
            $fieldSearch = $this->request->getQuery('fieldSearch');

            switch ($fieldSearch) {
                //posicion de opción 'All'
                case 0:
                    $query = $this->CatRegistro->generalSearch($keyword);
                    break;
                //posicion de opción 'Title'
                case 1:
                    $query = $this->CatRegistro->titleSearch($keyword);
                    break;
                //posicion de opción 'Authors'
                case 2:
                    $query = $this->CatRegistro->authorSearch($keyword);
                    break;
                //posicion de opción 'Abstract'
                case 3:
                    $query = $this->CatRegistro->locationSearch($keyword);
                    break;
                case 4:
                    $query = $this->CatRegistro->keywordSearch($keyword);
                    break;
                default:
                    $query = $this->CatRegistro->generalSearch($keyword);
                    break;
            }

            $this->set('catRegistro', $this->paginate($query));
            $this->set('_serialize', ['catRegistro']);
        }
    }
}
