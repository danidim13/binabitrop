<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatRegistro[]|\Cake\Collection\CollectionInterface $catRegistro
 * @author André Flasterstein
 */
?>

<?php $this->Paginator->setTemplates(['sort' => '<a class="text-white" href="{{url}}">{{text}}</a>']) ?>
<?php $this->Paginator->setTemplates(['sortAsc' => '<a class="text-white" href="{{url}}"><u>{{text}}</u></a>']) ?>
<?php $this->Paginator->setTemplates(['sortDesc' => '<a class="text-white" href="{{url}}"><u>{{text}}</u></a>']) ?>

<div class="mt-5 table-content">
    <table cellpadding="0" cellspacing="0" class="table table-striped table-hover">
        <thead class="bg-primary">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('TITULO', 'Title') ?></th>
            <th scope="col"><?= $this->Paginator->sort('AUTORES_PERSONAL', 'Author') ?></th>
            <th scope="col"><?= $this->Paginator->sort('DISPONIBILIDAD', 'Available') ?></th>
            <th scope="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($catRegistro as $catRegistro): ?>
        <tr style="cursor: pointer;">
            <td class="w-50"><?= h($catRegistro->TITULO) ?></td>
            <td class="w-25"><?= h($catRegistro->AUTORES_PERSONAL) ?></td>
            <?php if ($catRegistro->DISPONIBILIDAD) { ?>
            <td class="w-auto">Yes</td>
            <?php } else { ?>
            <td class="w-auto">No</td>
            <?php } ?>
            <td class="actions w-auto">
                <?= $this->Html->link(__('View'), ['action' => 'view', $catRegistro->ID_REGISTRO, $catRegistro->TIPO_DE_MATERIAL], ['class' => 'd-none']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $catRegistro->ID_REGISTRO], ['class' => 'pr-5']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $catRegistro->ID_REGISTRO], ['confirm' => __('Are you sure you want to delete # {0}?', $catRegistro->ID_REGISTRO)]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

<script>
    $('document').ready(function(){
        $('.table-content tr').click(function() {
            var href = $(this).find("a").attr("href");
            if(href) {
                window.location = href;
            }
        });
    });
</script>