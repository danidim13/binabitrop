<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatRegistro $catRegistro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cat Registro'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="catRegistro form large-9 medium-8 columns content">
    <?= $this->Form->create($catRegistro) ?>
    <fieldset>
        <legend><?= __('Add Cat Registro') ?></legend>
        <?php
            echo $this->Form->control('DISPONIBILIDAD');
            echo $this->Form->control('ACRONIMO_CENTRO_PARTICIPANTE');
            echo $this->Form->control('AFILIACION');
            echo $this->Form->control('AUTOR_INST_DOC_PADRE');
            echo $this->Form->control('AUTOR_INSTITUCIONAL');
            echo $this->Form->control('AUTOR_PERSONAL_DOC_PADRE');
            echo $this->Form->control('AUTORES_INSTITUCIONALES');
            echo $this->Form->control('AUTORES_PERSONALES');
            echo $this->Form->control('DESCRIPTORES_GEOGRAFICOS');
            echo $this->Form->control('DESCRIPTORES_LOCALES');
            echo $this->Form->control('DESCRIPTORES_PRIMARIOS');
            echo $this->Form->control('DESCRIPTORES_SECUNDARIOS');
            echo $this->Form->control('DOCUMENTALISTA_S');
            echo $this->Form->control('FECHA_INGRESO_REGISTRO');
            echo $this->Form->control('FECHA_PUBL_EDICION_FROM_ISO');
            echo $this->Form->control('FECHA_PUBL_EDICION_IN_EXTENSO');
            echo $this->Form->control('IDREG');
            echo $this->Form->control('ISSN2');
            echo $this->Form->control('MENCION_DE_LAS_PARTES');
            echo $this->Form->control('N_REGISTROS_DE_LAS_PARTES');
            echo $this->Form->control('RESUMEN');
            echo $this->Form->control('SIGNATURA_TOPOGRAFICA');
            echo $this->Form->control('TIPO_DE_MATERIAL');
            echo $this->Form->control('TITULO');
            echo $this->Form->control('UBICACION');
            echo $this->Form->control('NIVEL_BIBLIOGR_DOC_PADRE');
            echo $this->Form->control('NIVEL_BIBLIOGRAFICO');
            echo $this->Form->control('NOTAS');
            echo $this->Form->control('URL_OTS');
            echo $this->Form->control('PDF');
            echo $this->Form->control('ACTIVO');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
