<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatRegistro[]|\Cake\Collection\CollectionInterface $catRegistro
 * @author André Flasterstein
 */
?>
<h3 class="pt-3" ><?= __('BINABITROP Search') ?></h3>

<h7>Enter your search terms (e.g., <b>insect taxonomy</b> )</h7>
<div class="input-group">
    <?php echo $this->Form->text('search', ['class' => 'mt-3 col-5', 'label'=> false, 'id' => 'search']); ?>
    <span class="col-2">
        <?php echo $this->Form->input('searchField',array('id'=>'searchField', 'label'=>false, 'type'=>'select', 'class'=>'col-12 mt-3','options'=>['All','Title', 'Authors', 'Location', 'Keywords'])); ?>
    </span>
    <span class="col-1">
        <?php echo $this->Form->button('<i class="fa fa-search"></i>', ['id' => 'submit' ,'type' => 'submit', 'class' => 'col-12 mt-3 mb-3 primary']); ?>
    </span>
</div>

<?php $this->Paginator->setTemplates(['sort' => '<a class="text-white" href="{{url}}">{{text}}</a>']) ?>
<?php $this->Paginator->setTemplates(['sortAsc' => '<a class="text-white" href="{{url}}"><u>{{text}}</u></a>']) ?>
<?php $this->Paginator->setTemplates(['sortDesc' => '<a class="text-white" href="{{url}}"><u>{{text}}</u></a>']) ?>

<div class="mt-5 table-content">
    <table cellpadding="0" cellspacing="0" class="table table-striped table-hover">
        <thead class="bg-primary">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('TITULO', 'Title') ?></th>
            <th scope="col"><?= $this->Paginator->sort('AUTORES_PERSONAL', 'Author') ?></th>
            <th scope="col"><?= $this->Paginator->sort('DISPONIBILIDAD', 'Available') ?></th>
            <th scope="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($catRegistro as $catRegistro): ?>
        <tr style="cursor: pointer;">
            <td class="w-50"><?= h($catRegistro->TITULO) ?></td>
            <td class="w-25"><?= h($catRegistro->AUTORES_PERSONAL) ?></td>
            <?php if ($catRegistro->DISPONIBILIDAD) { ?>
                <td class="w-auto">Yes</td>
            <?php } else { ?>
                <td class="w-auto">No</td>
            <?php } ?>
            <td class="actions w-auto">
                <?= $this->Html->link(__('View'), ['action' => 'view', $catRegistro->ID_REGISTRO, $catRegistro->TIPO_DE_MATERIAL], ['class' => 'd-none']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $catRegistro->ID_REGISTRO], ['class' => 'pr-5']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $catRegistro->ID_REGISTRO], ['confirm' => __('Are you sure you want to delete # {0}?', $catRegistro->ID_REGISTRO)]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
<h7>National Bibliography on Tropical Biology (BINABITROP), contains published literature about Costa Rica. As of today, the database includes more than 42000 references to theses, dissertations, monographs, journals, congresses, etc. The biological concept (sensus latus) used includes agricultural, forestry and environmental sciences, natural resources, and veterinary medicine.</h7>

<script>
    $('document').ready(function(){
        var button = document.getElementById('submit');
        //Makes table rows clickable and redirects to register view.
        $('.table-content tr').click(function() {
            var href = $(this).find("a").attr("href");
            if(href) {
                window.location = href;
            }
        });

        //Enables search button.
        button.addEventListener('click', function(){
            var searchkey = $('#search').val();
            var searchField = $('#searchField').val();
            if(searchkey !== null && searchkey !== '') {
                //Triggers ajax to execute the search and fill the table with the corresponding results.
                searchRegister( searchkey, searchField );
            }
        }, false);

        //executes controller method that queries the database and fills table with the resulting data.
        function searchRegister( keyword, field ){
            var data = keyword;
            var fieldSearch = field;
            $.ajax({
                method: 'get',
                url : "<?php echo $this->Url->build( [ 'controller' => 'CatRegistro', 'action' => 'lookup' ] ); ?>",
                data: {keyword:data, fieldSearch:field},
                success: function( response )
                {
                    $('.paginator').remove();
                    $( '.table-content' ).html(response);
                }
            });
        };
    });
</script>