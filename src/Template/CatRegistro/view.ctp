<?php
/**
 * @author Fabián Álvarez
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatRegistro $catRegistro
 */
?>
<div class="container">

    <!-- Titles section -->
    <div class="mb-5">
        <div class="row justify-content-center mb-3">
            <h3><?= h($catRegistro->TITULO) ?></h3>
        </div>
        <div class="row justify-content-center">
            <h5><?= h($catRegistro->TITULO) ?></h5>
        </div>
    </div>

    <div>
        <div class="row">
            <div class="col-2">
                <strong>Author(s):</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->AUTORES_PERSONAL)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Publication Date:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->FECHA_PUBL_IN_EXT)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Affiliation:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->AFILIACION)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Notes:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->NOTAS)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Primary Descriptors:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->DESCRIPT_PRIMAR)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Secondary Descriptors:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->DESCRIPT_SECUND)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Geographic Descriptors:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->DESCRIPT_GEOG)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Local Descriptors:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->DESCRIPT_LOCALES)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Location:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->UBICACION)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <div class="row">
            <div class="col-2">
                <strong>Summary:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <?= h($catRegistro->RESUMEN)?>
            </div>
        </div>
        <div class="row bg-primary my-4" style="height: 2px;"></div>

        <?php if ($catRegistro->TIPO_DE_MATERIAL === 'PER'): ?>


            <div class="row">
                <div class="col-2">
                    <strong>Other Notes:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->NOTAS)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Series Title 2:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->TITULO_DE_LA_SERIADA2)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>ISSN:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->ISSN)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Parties Mention:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->MENCION_DE_LAS_PARTES)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>


        <?php elseif ($catRegistro->TIPO_DE_MATERIAL === 'CONF'): ?>


            <div class="row">
                <div class="col-2">
                    <strong>M C Collation:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->COLACION_M_C)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Other Notes:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->NOTAS)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>ES Reunion:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->REUNION_ES)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>


        <?php elseif ($catRegistro->TIPO_DE_MATERIAL === 'MONO'): ?>


            <div class="row">
                <div class="col-2">
                    <strong>M C Collation:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->COLACION_M_C)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Publishing House:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->EDITORIAL)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Other Notes:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->NOTAS)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Monograph/Series:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->SERIES_MONOG)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>


        <?php elseif ($catRegistro->TIPO_DE_MATERIAL === 'PART'): ?>


            <div class="row">
                <div class="col-2">
                    <strong>M C Collation:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->COLACION_M_C)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Publishing House:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->EDITORIAL)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>ISBN S:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->ISBN_S)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>M C Parent Document Title:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->TITULO_DOC_PADRE_M_C)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>


        <?php elseif ($catRegistro->TIPO_DE_MATERIAL === 'THES'): ?>


            <div class="row">
                <div class="col-2">
                    <strong>M C Collation:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->COLACION_M_C)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Publishing House:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->EDITORIAL)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Other Notes:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->NOTAS)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>

            <div class="row">
                <div class="col-2">
                    <strong>Thesis:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= h($catEspecializacion->TESIS)?>
                </div>
            </div>
            <div class="row bg-primary my-4" style="height: 2px;"></div>
        <?php endif; ?>
    </div>

    <div class="row justify-content-end">
        <?= $this->Html->link('Return', ['action' => 'search'], ['class' => 'btn btn-outline-primary font-weight-bold'])?>
    </div>
</div>