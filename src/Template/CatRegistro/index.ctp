<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatRegistro[]|\Cake\Collection\CollectionInterface $catRegistro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Cat Registro'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="catRegistro index large-9 medium-8 columns content">
    <h3><?= __('Cat Registro') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID_REGISTRO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('DISPONIBILIDAD') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ACRONIMO_CENTRO_PARTICIPANTE') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AFILIACION') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AUTOR_INST_DOC_PADRE') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AUTOR_INSTITUCIONAL') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AUTOR_PERSONAL_DOC_PADRE') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AUTORES_INSTITUCIONALES') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AUTORES_PERSONALES') ?></th>
                <th scope="col"><?= $this->Paginator->sort('DESCRIPTORES_GEOGRAFICOS') ?></th>
                <th scope="col"><?= $this->Paginator->sort('DESCRIPTORES_LOCALES') ?></th>
                <th scope="col"><?= $this->Paginator->sort('DESCRIPTORES_PRIMARIOS') ?></th>
                <th scope="col"><?= $this->Paginator->sort('DESCRIPTORES_SECUNDARIOS') ?></th>
                <th scope="col"><?= $this->Paginator->sort('DOCUMENTALISTA_S') ?></th>
                <th scope="col"><?= $this->Paginator->sort('FECHA_INGRESO_REGISTRO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('FECHA_PUBL_EDICION_FROM_ISO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('FECHA_PUBL_EDICION_IN_EXTENSO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IDREG') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ISSN2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('MENCION_DE_LAS_PARTES') ?></th>
                <th scope="col"><?= $this->Paginator->sort('N_REGISTROS_DE_LAS_PARTES') ?></th>
                <th scope="col"><?= $this->Paginator->sort('SIGNATURA_TOPOGRAFICA') ?></th>
                <th scope="col"><?= $this->Paginator->sort('TIPO_DE_MATERIAL') ?></th>
                <th scope="col"><?= $this->Paginator->sort('TITULO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('UBICACION') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NIVEL_BIBLIOGR_DOC_PADRE') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NIVEL_BIBLIOGRAFICO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NOTAS') ?></th>
                <th scope="col"><?= $this->Paginator->sort('URL_OTS') ?></th>
                <th scope="col"><?= $this->Paginator->sort('PDF') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ACTIVO') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($catRegistro as $catRegistro): ?>
            <tr>
                <td><?= $this->Number->format($catRegistro->ID_REGISTRO) ?></td>
                <td><?= h($catRegistro->DISPONIBILIDAD) ?></td>
                <td><?= h($catRegistro->ACRONIMO_CENTRO_PARTICIPANTE) ?></td>
                <td><?= h($catRegistro->AFILIACION) ?></td>
                <td><?= h($catRegistro->AUTOR_INST_DOC_PADRE) ?></td>
                <td><?= h($catRegistro->AUTOR_INSTITUCIONAL) ?></td>
                <td><?= h($catRegistro->AUTOR_PERSONAL_DOC_PADRE) ?></td>
                <td><?= h($catRegistro->AUTORES_INSTITUCIONALES) ?></td>
                <td><?= h($catRegistro->AUTORES_PERSONALES) ?></td>
                <td><?= h($catRegistro->DESCRIPTORES_GEOGRAFICOS) ?></td>
                <td><?= h($catRegistro->DESCRIPTORES_LOCALES) ?></td>
                <td><?= h($catRegistro->DESCRIPTORES_PRIMARIOS) ?></td>
                <td><?= h($catRegistro->DESCRIPTORES_SECUNDARIOS) ?></td>
                <td><?= $this->Number->format($catRegistro->DOCUMENTALISTA_S) ?></td>
                <td><?= h($catRegistro->FECHA_INGRESO_REGISTRO) ?></td>
                <td><?= h($catRegistro->FECHA_PUBL_EDICION_FROM_ISO) ?></td>
                <td><?= h($catRegistro->FECHA_PUBL_EDICION_IN_EXTENSO) ?></td>
                <td><?= h($catRegistro->IDREG) ?></td>
                <td><?= h($catRegistro->ISSN2) ?></td>
                <td><?= h($catRegistro->MENCION_DE_LAS_PARTES) ?></td>
                <td><?= h($catRegistro->N_REGISTROS_DE_LAS_PARTES) ?></td>
                <td><?= h($catRegistro->SIGNATURA_TOPOGRAFICA) ?></td>
                <td><?= h($catRegistro->TIPO_DE_MATERIAL) ?></td>
                <td><?= h($catRegistro->TITULO) ?></td>
                <td><?= h($catRegistro->UBICACION) ?></td>
                <td><?= h($catRegistro->NIVEL_BIBLIOGR_DOC_PADRE) ?></td>
                <td><?= h($catRegistro->NIVEL_BIBLIOGRAFICO) ?></td>
                <td><?= h($catRegistro->NOTAS) ?></td>
                <td><?= h($catRegistro->URL_OTS) ?></td>
                <td><?= h($catRegistro->PDF) ?></td>
                <td><?= h($catRegistro->ACTIVO) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $catRegistro->ID_REGISTRO]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $catRegistro->ID_REGISTRO]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $catRegistro->ID_REGISTRO], ['confirm' => __('Are you sure you want to delete # {0}?', $catRegistro->ID_REGISTRO)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
