<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegUsuario[]|\Cake\Collection\CollectionInterface $segUsuario
 */
use Cake\Routing\Router;
?>

<?php $this->Paginator->setTemplates(['sort' => '<a class="text-white" href="{{url}}">{{text}}</a>']) ?>
<?php $this->Paginator->setTemplates(['sortAsc' => '<a class="text-white" href="{{url}}"><u>{{text}}</u></a>']) ?>
<?php $this->Paginator->setTemplates(['sortDesc' => '<a class="text-white" href="{{url}}"><u>{{text}}</u></a>']) ?>

<h3>Manage Users</h3>
<div class="mt-5 table-content">
<table cellpadding="0" cellspacing="0" class="table table-striped">
    <thead class="bg-primary">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('ID_USUARIO', __('User Id')) ?></th>
            <th scope="col"><?= $this->Paginator->sort('NOMBRE_USUARIO',__('User Name')) ?></th>
            <th scope="col"><?= $this->Paginator->sort('ROL_FK',__('Role')) ?></th>
            <th scope="col"><?= $this->Paginator->sort('NOMBRE',__('Name')) ?></th>
            <th scope="col"><?= $this->Paginator->sort('PRIMER_APELLIDO',__('Last Name')) ?></th>
            <th scope="col"><?= $this->Paginator->sort('CORREO_ELECTRONICO',__('Email')) ?>  </th>
            <th scope="col"><span class="text-white"><?= __('Actions') ?></span></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($segUsuario as $segUsuario): ?>
        <tr>
            <td><?= $this->Number->format($segUsuario->ID_USUARIO) ?></td>
            <td><?= h($segUsuario->NOMBRE_USUARIO) ?></td>
            <td><?= h($segUsuario->ROL_FK) ?></td>
            <td><?= h($segUsuario->NOMBRE) ?></td>
            <td><?= h($segUsuario->PRIMER_APELLIDO) ?> <?= h($segUsuario->SEGUNDO_APELLIDO) ?></td>
            <td><?= h($segUsuario->CORREO_ELECTRONICO) ?></td>
            <td>
                <span><?= $this->Html->link(__(''), ['action' => 'view', $segUsuario->ID_USUARIO], ["class" => "fas fa-info-circle"]) ?></span>
                <span><?= $this->Html->link(__(''), ['action' => 'edit', $segUsuario->ID_USUARIO],["class" => "fas fa-edit"]) ?></span>
                <?php
                    echo $this->Html->link(
                        $this->Html->tag('i', '', ['class'=>'fas fa-trash-alt']),
                        '#',
                        array(
                            array('class' => 'glyphicon glyphicon-trash'),
                            'data-toggle'=> 'modal',
                            'data-target' => '#ConfirmDelete',
                            'data-action'=> Router::url(
                                array('action'=>'delete',$segUsuario->ID_USUARIO)
                            ),
                            'escape' => false),
                        false);
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

<!-- Modal -->
<div class="modal fade" id="ConfirmDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"><?=__('Are you sure you want to delete this user?', $segUsuario->id_usuario)?></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary text-white px-3" data-dismiss="modal">Cancel</button>
                <?php
                echo $this->Form->postLink(
                    'Confirm',
                    array('action' => 'delete'),
                    array('class' => 'btn btn-secondary text-white px-3'),
                    false
                );
                ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
    $('#ConfirmDelete').on('show.bs.modal', function(e) {
        $(this).find('form').attr('action', $(e.relatedTarget).data('action'));
    });
</script>