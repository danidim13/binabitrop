<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegUsuario $segUsuario
 */
?>

<div class="d-flex flex-column align-items-center my-3 py-3">
    <div class="d-flex flex-column col-sm-4 border rounded shadow m-4 p-4 ">
        <?= $this->Form->create($segUsuario, ['context' => ['validator' => 'login']]) ?>
        <fieldset>
            <div class="my-4">
            </div>
            <div class="form-group form-row p-3">
                <label for="username" class="font-weight-bold"><?= __('Username') ?></label>
                <?= $this->Form->text('username')?>  
            </div>
            <div class="form-group form-row p-3">
                <label for="password" class="font-weight-bold"><?= __('Password') ?></label>
                <?= $this->Form->password('password')?> 
                <small class="py-2"><?= $this->Html->link(__('Forgot your password?'), '#') ?></small>
            </div>
        </fieldset>
        <div class="my-2">
        </div>
        <div class="d-flex flex-column align-items-center">
            <?= $this->Form->button(__('Sign in'), ['class' => 'btn btn-primary text-white px-5']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>