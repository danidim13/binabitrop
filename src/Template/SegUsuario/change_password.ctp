<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegUsuario $segUsuario
 */
?>

<div class="d-flex container justify-content-center col-6 border shadow ">
    <?= $this->Form->create($segUsuario, ['context' => ['validator' => 'password']]) ?>
    <br>
    <fieldset>
        <legend><?= __('Change your password') ?></legend>
        <?= $this->Form->control('password',['label'=>'Password']); ?>
        <?= $this->Form->input ('password_conf', ['label'=>'Confirm your password', 'type'=>'password']); ?>
    </fieldset>
    <div class="row form-group">
        <div class="col-md-3">
            <div class="col-md-6">
                <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary text-white px-3']) ?>
            </div>
        </div>
        <div class="col-md-3 pull-right">
        </div>
        <div class="col-md-3">
            <div class="col-md-12">
                <?= $this->Html->link(__('Cancel'), ['controller' => 'SegUsuario', 'action' => 'view', $segUsuario->ID_USUARIO], ['class' => 'btn btn-secondary text-white px-3'])?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>



</div>