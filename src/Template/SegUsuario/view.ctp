<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegUsuario $segUsuario
 */

use Cake\Routing\Router;
?>
<h3> Manage Users</h3>
<div class="mt-5">
<table cellpadding="0" cellspacing="0" class="table table-striped">
<tr>
        <th scope="row"><?= __('Id User') ?></th>
        <td><?= h($segUsuario->ID_USUARIO) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Role') ?></th>
        <td><?= h($segUsuario->ROL_FK) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Name') ?></th>
        <td><?= h($segUsuario->NOMBRE) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Last Name') ?></th>
        <td><?= h($segUsuario->PRIMER_APELLIDO) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Email') ?></th>
        <td><?=  h($segUsuario->CORREO_ELECTRONICO)  ?></td>
    </tr>
</table>
</div>
<div class = "d-flex flex-row justify-content-between">
    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $segUsuario->ID_USUARIO], ['class' => 'btn btn-outline-primary mr-4 px-4']) ?>
    <?= $this->Html->link(__('Change password'), ['action' => 'changePassword'], ['class' => 'btn btn-outline-warning mx-4']) ?>
    <?php
        echo $this->Html->link(__('Delete Account'),
            '#',
            [
                'class' => 'btn btn-secondary mr-4 px-4',
                'data-toggle' => 'modal',
                'data-target' => '#ConfirmDelete',
                'data-action' => Router::url(['action' =>'delete',$segUsuario->ID_USUARIO]),
                'escape' => false
            ]);
    ?>
</div>


<!-- Modal -->
<div class="modal fade" id="ConfirmDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"><?=__('Are you sure you want to delete this user?', $segUsuario->id_usuario)?></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary text-white px-3" data-dismiss="modal">Cancel</button>
                <?php
                echo $this->Form->postLink(
                    'Confirm',
                    array('action' => 'delete'),
                    array('class' => 'btn btn-secondary text-white px-3'),
                    false
                );
                ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script>
    $('#ConfirmDelete').on('show.bs.modal', function(e) {
        $(this).find('form').attr('action', $(e.relatedTarget).data('action'));
    });
</script>
