<?php
/**
 * Vista de Registro del usuario al sistema.
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegUsuario $segUsuario
 * @author Adrian Alvarez
 */
?>
<div class="d-flex flex-column align-items-center my-3 py-3">
    <div class="d-flex flex-column col-6 border rounded shadow m-4 p-4">
    <?= $this->Form->create($segUsuario, ['context' => ['validator' => 'register']]) ?>
        <legend align="center"><?= __('Register to BINABITROP') ?></legend>
    <fieldset>
    <?php
        echo $this->Form->control('NOMBRE_USUARIO', ['class' => 'form-control' , 'label' => ['class'=>'font-weight-bold', 'text'=>__('Username')], 'pattern' => '(^[a-zA-Z])([a-zA-Z0-9\_\-\.]){2,29}', 'title' => __('Username format:
             -It should contain at least 3 characters.
             -Username must start with a letter.
             -It can\'t contain more than 30 characters.')]);?>
        <?php echo $this->Form->control('NOMBRE',['class' => 'form-control', 'label' => ['class'=>'font-weight-bold', 'text'=>__('Name')], 'pattern' => '^([a-zA-Z]+)([a-z A-Z])*', 'title' => __('Name format: It should only contain letters or whitespace')]);
        ?>
        <?php echo $this->Form->control('PRIMER_APELLIDO', ['class' => 'form-control' , 'label'=>['class'=>'font-weight-bold',
        'text'=>__('Last Name')], 'pattern' => '^([a-zA-Z]+)([a-z A-Z])',
        'title' => __('Last name format: It should only contain letters or whitespace')]);
        ?>
        <?php echo $this->Form->control('CORREO_ELECTRONICO',['class' => 'form-control' , 'label'=>['class'=>'font-weight-bold', 'text'=>__('E-mail')], 'title' => __('It must be a valid e-mail format.')]);
        ?>
        <?php echo $this->Form->input('password',['class' => 'form-control' , 'label'=>['class'=>'font-weight-bold', 'text'=>__('Password')], 'title' => ['text'=>__('Passwords need to have:
           - 1 uppercase letter
           - 1 lowercase letter
           - 1 numeral
           - 1 special character')]]);
        ?>
        <?php echo $this->Form->input ('password_conf', ['class' => 'form-control' , 'label'=>['class'=>'font-weight-bold', 'text'=>__('Confirm your password')], 'type'=>'password', 'title' => __('Passwords need to have:
        - 1 uppercase letter
        - 1 lowercase letter
        - 1 numeral
        - 1 special character')]);
        ?>
    </fieldset>
    <br>
    <div class="d-flex container justify-content-center">
        <div class="col-md-6" align="center">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary text-white px-3']) ?>
        </div>
        <div class="col-md-6" align="center">
            <?= $this->Html->link(__('Cancel'), ['controller' => 'CatRegistro', 'action' => 'search'], ['class' => 'btn btn-secondary text-white px-3'])?>
        </div>
        <br>
        <br>
    </div>
    <?= $this->Form->end() ?>



</div>


