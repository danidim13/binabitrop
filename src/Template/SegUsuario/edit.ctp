<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegUsuario $segUsuario
 */
?>
<div class="segUsuario form large-9 medium-8 columns content ">
    <?= $this->Form->create($segUsuario) ?>
    <fieldset>
        <legend><?= __('Edit User ')?></legend>
        <?php
            echo $this->Form->control('NOMBRE_USUARIO', ['readonly', 'class'=>'form-control-paintext', 'label' => ['class'=>'font-weight-bold', 'text'=>__('Username')]]); ?>
            <br>
            <br>
        <?php
            echo $this->Form->control('NOMBRE', ['class'=>'form-control', 'label' => ['class'=>'font-weight-bold', 'text'=>__('Name')]]); ?>
            <br>
            <br>
            <?php  echo $this->Form->control(__('PRIMER_APELLIDO'), ['class'=>'form-control', 'label' => ['class'=>'font-weight-bold', 'text'=>__('Last Name')]]); ?>
            <br>
            <br>
            <?php echo $this->Form->control('CORREO_ELECTRONICO', ['class'=>'form-control', 'label' => ['class'=>'font-weight-bold', 'text'=>__('Email')]]); ?>
            <br>
            <br>
        </fieldset>
    <?= $this->Form->button(__('Submit'), ['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link(__('Return'), ['action' => 'view', $segUsuario->ID_USUARIO], ['class' => 'btn btn-outline-primary ml-4'])?>
    <?= $this->Form->end() ?>
</div>
