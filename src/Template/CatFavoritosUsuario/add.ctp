<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatFavoritosUsuario $catFavoritosUsuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cat Favoritos Usuario'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="catFavoritosUsuario form large-9 medium-8 columns content">
    <?= $this->Form->create($catFavoritosUsuario) ?>
    <fieldset>
        <legend><?= __('Add Cat Favoritos Usuario') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
