<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatFavoritosUsuario $catFavoritosUsuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cat Favoritos Usuario'), ['action' => 'edit', $catFavoritosUsuario->USUARIO_FK]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cat Favoritos Usuario'), ['action' => 'delete', $catFavoritosUsuario->USUARIO_FK], ['confirm' => __('Are you sure you want to delete # {0}?', $catFavoritosUsuario->USUARIO_FK)]) ?> </li>
        <li><?= $this->Html->link(__('List Cat Favoritos Usuario'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cat Favoritos Usuario'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="catFavoritosUsuario view large-9 medium-8 columns content">
    <h3><?= h($catFavoritosUsuario->USUARIO_FK) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('USUARIO FK') ?></th>
            <td><?= $this->Number->format($catFavoritosUsuario->USUARIO_FK) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('REGISTRO FK') ?></th>
            <td><?= $this->Number->format($catFavoritosUsuario->REGISTRO_FK) ?></td>
        </tr>
    </table>
</div>
