<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CatFavoritosUsuario[]|\Cake\Collection\CollectionInterface $catFavoritosUsuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Cat Favoritos Usuario'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="catFavoritosUsuario index large-9 medium-8 columns content">
    <h3><?= __('Cat Favoritos Usuario') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('USUARIO_FK') ?></th>
                <th scope="col"><?= $this->Paginator->sort('REGISTRO_FK') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($catFavoritosUsuario as $catFavoritosUsuario): ?>
            <tr>
                <td><?= $this->Number->format($catFavoritosUsuario->USUARIO_FK) ?></td>
                <td><?= $this->Number->format($catFavoritosUsuario->REGISTRO_FK) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $catFavoritosUsuario->USUARIO_FK]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $catFavoritosUsuario->USUARIO_FK]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $catFavoritosUsuario->USUARIO_FK], ['confirm' => __('Are you sure you want to delete # {0}?', $catFavoritosUsuario->USUARIO_FK)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
