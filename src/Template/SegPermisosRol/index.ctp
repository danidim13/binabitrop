<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegPermisosRol[]|\Cake\Collection\CollectionInterface $segPermisosRol
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Seg Permisos Rol'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="segPermisosRol index large-9 medium-8 columns content">
    <h3><?= __('Seg Permisos Rol') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('PERMISOS_ROL') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ROL_FK') ?></th>
                <th scope="col"><?= $this->Paginator->sort('PERMISOS_FK') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($segPermisosRol as $segPermisosRol): ?>
            <tr>
                <td><?= $this->Number->format($segPermisosRol->PERMISOS_ROL) ?></td>
                <td><?= h($segPermisosRol->ROL_FK) ?></td>
                <td><?= $this->Number->format($segPermisosRol->PERMISOS_FK) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $segPermisosRol->PERMISOS_ROL]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $segPermisosRol->PERMISOS_ROL]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $segPermisosRol->PERMISOS_ROL], ['confirm' => __('Are you sure you want to delete # {0}?', $segPermisosRol->PERMISOS_ROL)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
