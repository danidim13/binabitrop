<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegPermisosRol $segPermisosRol
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Seg Permisos Rol'), ['action' => 'edit', $segPermisosRol->PERMISOS_ROL]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Seg Permisos Rol'), ['action' => 'delete', $segPermisosRol->PERMISOS_ROL], ['confirm' => __('Are you sure you want to delete # {0}?', $segPermisosRol->PERMISOS_ROL)]) ?> </li>
        <li><?= $this->Html->link(__('List Seg Permisos Rol'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Seg Permisos Rol'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="segPermisosRol view large-9 medium-8 columns content">
    <h3><?= h($segPermisosRol->PERMISOS_ROL) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ROL FK') ?></th>
            <td><?= h($segPermisosRol->ROL_FK) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PERMISOS ROL') ?></th>
            <td><?= $this->Number->format($segPermisosRol->PERMISOS_ROL) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PERMISOS FK') ?></th>
            <td><?= $this->Number->format($segPermisosRol->PERMISOS_FK) ?></td>
        </tr>
    </table>
</div>
