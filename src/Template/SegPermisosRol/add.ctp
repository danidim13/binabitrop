<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegPermisosRol $segPermisosRol
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Seg Permisos Rol'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="segPermisosRol form large-9 medium-8 columns content">
    <?= $this->Form->create($segPermisosRol) ?>
    <fieldset>
        <legend><?= __('Add Seg Permisos Rol') ?></legend>
        <?php
            echo $this->Form->control('ROL_FK');
            echo $this->Form->control('PERMISOS_FK');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
