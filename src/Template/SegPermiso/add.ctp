<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegPermiso $segPermiso
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Seg Permiso'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="segPermiso form large-9 medium-8 columns content">
    <?= $this->Form->create($segPermiso) ?>
    <fieldset>
        <legend><?= __('Add Seg Permiso') ?></legend>
        <?php
            echo $this->Form->control('ACCION');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
