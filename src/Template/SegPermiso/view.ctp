<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegPermiso $segPermiso
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Seg Permiso'), ['action' => 'edit', $segPermiso->ID_PERMISO]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Seg Permiso'), ['action' => 'delete', $segPermiso->ID_PERMISO], ['confirm' => __('Are you sure you want to delete # {0}?', $segPermiso->ID_PERMISO)]) ?> </li>
        <li><?= $this->Html->link(__('List Seg Permiso'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Seg Permiso'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="segPermiso view large-9 medium-8 columns content">
    <h3><?= h($segPermiso->ID_PERMISO) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ACCION') ?></th>
            <td><?= h($segPermiso->ACCION) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID PERMISO') ?></th>
            <td><?= $this->Number->format($segPermiso->ID_PERMISO) ?></td>
        </tr>
    </table>
</div>
