<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegPermiso[]|\Cake\Collection\CollectionInterface $segPermiso
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Seg Permiso'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="segPermiso index large-9 medium-8 columns content">
    <h3><?= __('Seg Permiso') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID_PERMISO') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ACCION') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($segPermiso as $segPermiso): ?>
            <tr>
                <td><?= $this->Number->format($segPermiso->ID_PERMISO) ?></td>
                <td><?= h($segPermiso->ACCION) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $segPermiso->ID_PERMISO]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $segPermiso->ID_PERMISO]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $segPermiso->ID_PERMISO], ['confirm' => __('Are you sure you want to delete # {0}?', $segPermiso->ID_PERMISO)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
