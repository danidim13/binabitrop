<?php
/**
 * Barra de navegación.
 * @param array $navbarLinks Array con las acciones o enlaces que se deben desplegar
 * @author          Daniel Díaz
 * @copyright       Copyright (c) Organizacion para Estudios Tropicales
 */
?>
<nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-lg flox-md-row">
    <ul class="navbar-nav ml-4 h4">
        <?php foreach ($navbarLinks as $link): ?>
        <li class="nav-item mx-2">
            <?= $this->Html->link($link['text'], $link['link'], ['class' => ($link['active'] === true ? 'nav-link active' : 'nav-link') ]); ?>
        </li>
        <?php endforeach; ?>
    </ul>
</nav>