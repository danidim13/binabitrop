<?php
/**
 * Header, incluye el logo y el menú usuario/login.
 * @param array|bool $logged Indica si el usuario está loggeado
 * @author          Daniel Díaz
 * @copyright       Copyright (c) Organizacion para Estudios Tropicales
 */
?>

<nav class="navbar navbar-expand-md navbar-dark bg-success flex-column flex-lg-row">
    <a class="navbar-brand" href="https://tropicalstudies.org/">
        <?= $this->Html->image('logo_white.png') ?>
    </a>
    <div class="ml-auto">
        <?php if($logged): ?>
            <div class="dropdown mr-3">
                <a href="#" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $this->Html->icon('user-circle', ['class'=>'text-primary', 'style' => 'font-size:40px;'] ) ?>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labeledby="dropdownUser">
                    <?= $this->Html->link(__('Profile'), ['controller' => 'SegUsuario', 'action' => 'view', $logged['ID_USUARIO'] ], ['class' => 'dropdown-item']) ?>
                    <?= $this->Html->link(__('Log out'), ['controller' => 'SegUsuario', 'action' => 'logout'], ['class' => 'dropdown-item']) ?>
                </div>
            </div>
        <?php else: ?>
            <?= $this->Html->link('Log in', ['controller' => 'segUsuario', 'action' => 'login'], ['class' => 'navbar-text text-white d-inline-block mr-3']); ?>
            <?= $this->Html->link('Sign up', ['controller' => 'segUsuario', 'action' => 'add'], ['class' => 'btn btn-success text-white border border-white shadow mr-3']); ?>
        <?php endif;?>
    </div>
</nav>

