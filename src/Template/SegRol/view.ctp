<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegRol $segRol
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Seg Rol'), ['action' => 'edit', $segRol->ID_ROL]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Seg Rol'), ['action' => 'delete', $segRol->ID_ROL], ['confirm' => __('Are you sure you want to delete # {0}?', $segRol->ID_ROL)]) ?> </li>
        <li><?= $this->Html->link(__('List Seg Rol'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Seg Rol'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="segRol view large-9 medium-8 columns content">
    <h3><?= h($segRol->ID_ROL) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ID ROL') ?></th>
            <td><?= h($segRol->ID_ROL) ?></td>
        </tr>
    </table>
</div>
