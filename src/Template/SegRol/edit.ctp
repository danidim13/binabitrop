<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SegRol $segRol
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $segRol->ID_ROL],
                ['confirm' => __('Are you sure you want to delete # {0}?', $segRol->ID_ROL)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Seg Rol'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="segRol form large-9 medium-8 columns content">
    <?= $this->Form->create($segRol) ?>
    <fieldset>
        <legend><?= __('Edit Seg Rol') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
