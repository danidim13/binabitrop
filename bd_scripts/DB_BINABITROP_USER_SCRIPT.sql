/*
-Crea el schema de la bd si no existe y le da permisos.
-TODO: PONER UNA CONTRASEÑA DE VDD, ESTOY PROBANDO.
-ESTO SE HACE DESDE SYSDBA
*/
declare
userexist integer;
begin
  select count(*) into userexist from dba_users where username='BINABITROP';
  if (userexist = 0) then
    execute immediate 'CREATE USER BINABITROP
	IDENTIFIED BY oracle
	DEFAULT TABLESPACE USERS
	Temporary TABLESPACE TEMP;
	';
	
  end if;
end;
/

Grant all privileges to BINABITROP identified by oracle;