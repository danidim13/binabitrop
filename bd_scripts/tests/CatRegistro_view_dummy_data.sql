use bd_binabitrop;

insert into SEG_ROL (ID_ROL)
values ('rol');

insert into SEG_PERMISO (ID_PERMISO, ACCION)
values (0,'ACCION');

insert into SEG_PERMISOS_ROL (PERMISOS_ROL, ROL_FK, PERMISOS_FK)
values (0,'rol',0);

insert into SEG_USUARIO (ID_USUARIO, NOMBRE_USUARIO, ROL_FK, NOMBRE, PRIMER_APELLIDO, CORREO_ELECTRONICO, BCRYPT_HASH)
values (0,'Wakanda4Inge','rol','Ricky','Ricardo','wakanda4inge@aaa.com','asdfqwerzxcvtyuighjvbnm');

insert into CAT_REGISTRO (ID_REGISTRO, DISPONIBILIDAD, ACRON_CENTRO_PARTI, AFILICIACION, AUTOR_INST_DOC_PADRE, AUTOR_INSTITUCIONAL, AUTOR_PERSONAL_DOC_PADRE, AUTORES_INSTITUCIONALES, AUTORES_PERSONALES, DESCRIPTORES_GEOGRAFICOS, DESCRIPTORES_LOCALES, DESCRIPTORES_PRIMARIOS, DESCRIPTORES_SECUNDARIOS, DOCUMENTALISTA_S, FECHA_INGRESO_REGISTRO, FECHA_PUBL_EDICION_FROM_ISO, FECHA_PUBL_EDICION_IN_EXTENSO, IDREG, ISSN2, MENCION_DE_LAS_PARTES, N_REGISTROS_DE_LAS_PARTES, RESUMEN, SIGNATURA_TOPOGRAFICA, TIPO_DE_MATERIAL, TITULO, UBICACION, NIVEL_BIBLIOGR_DOC_PADRE, NIVEL_BIBLIOGRAFICO, NOTAS, URL_OTS, PDF)
values (0,'a','a','a','a','a','a','a','a','a','a','a','a',0,'a','a','a','a','a','a','a','a','a','PER','a','a','a','a','a','a','a'),
(1,'b','b','b','b','b','b','b','b','b','b','b','b',0,'b','b','b','b','b','b','b','b','b','CONF','b','b','b','b','b','b','b'),
(2,'c','c','c','c','c','c','c','c','c','c','c','c',0,'c','c','c','c','c','c','c','c','c','MONO','c','c','c','c','c','c','c'),
(3,'d','d','d','d','d','d','d','d','d','d','d','d',0,'d','d','d','d','d','d','d','d','d','PART','d','d','d','d','d','d','d'),
(4,'e','e','e','e','e','e','e','e','e','e','e','e',0,'e','e','e','e','e','e','e','e','e','THES','e','e','e','e','e','e','e');

insert into CAT_SERIADA (REGISTRO_FK, NOTAS, TITULO_DE_LA_SERIADA2, ISSN, MENCION_DE_LAS_PARTES)
values (0,'a','a','a','a');

insert into CAT_CONF_TALLER_SIMPOSIO (REGISTRO_FK, COLACION_M_C, NOTAS, REUNION_ES)
values (1,'b','b','b');

insert into CAT_MONOG_LIBRO (REGISTRO_FK, COLACION_M_C, EDITORIAL, NOTAS, SERIES_MONOG)
values (2,'c','c','c','c');

insert into CAT_P_MONOG_CAP_LIB (REGISTRO_FK, COLACION_M_C, EDITORIAL, ISBN_S, TITULO_DOC_PADRE_M_C)
values (3,'d','d','d','d');

insert into CAT_TESIS (REGISTRO_FK, COLACION_M_C, EDITORIAL, NOTAS, TESIS)
values (4,'e','e','e','e');